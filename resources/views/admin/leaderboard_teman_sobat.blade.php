@extends('crudbooster::admin_template')
@section('content')
<div class='panel panel-default'>
    <div class='panel-heading'>{{{$data["page_title"]}}}</div>
        <div class='panel-body'>
            <div class="table-responsive">
                <table id="table-detail" class="table table-striped">
                    <tbody>
                        <tr>
                            <th>Peringkat</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Whatsapp</th>
                            <th>Total teman Sobat</th>
                            <th>Status Spam</th>
                        </tr>
                        @php
                        $i = 1;
                        @endphp
                        @forelse($leaderboards as $key => $value)
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{$value->name}}</td>
                            <td>{{$value->email}}</td>
                            <td>{{$value->whatsapp}}</td>
                            <td>{{$value->amount}}</td>
                            <td>{{$value->spam == 0? "Tidak Spam":"spam"}}</td>
                        </tr>
                        <?php $i++; ?>
                        @empty
                        <tr>
                            <td colspan="6">Data Kosong</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
