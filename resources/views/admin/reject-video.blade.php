@extends('crudbooster::admin_template')
@section('content')
<div class='panel panel-default'>
    <div class='panel-heading'>Detail User</div>
        <div class='panel-body'>
            <div class="table-responsive">
                <table id="table-detail" class="table table-striped">
                    <tbody>
                        <tr>
                            <th>Name</th>
                            <td>{{$row->name}}</td>
                        </tr>
                        <tr>
                            <th>Video</th>
                            <td><a target="_blank" href="{{$row->original_link}}">Klik Disini</a></td>
                        </tr>
                        <form class="" action="{{route('rejectSppaVideo')}}" method="post">
                          @csrf
                          <tr>
                            <th>Alasan Penolakan</th>
                            <td>
                              <textarea class="form-control" rows="3" name="reason" placeholder="Enter ..." required></textarea>
                            </td>
                          </tr>
                          <tr>
                            <th></th>
                            <td><button type="submit" class="btn btn-primary">Tolak</button></td>
                          </tr>
                          <input type="hidden" name="video_id" value="{{$row->video_id}}">
                        </form>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
