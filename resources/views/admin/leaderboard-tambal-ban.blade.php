@extends("crudbooster::admin_template")

@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="{{ set_active('panjatLeaderboard') }}"><a href="{{ route('panjatLeaderboard') }}">Panjat Pinang</a></li>
              <li class="{{ set_active('leaderboardTambalBan') }}"><a href="{{ route('leaderboardTambalBan') }} ">Tambal Ban</a></li>
            </ul>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Per transaction</h3>
                    </div>
                     <!--/.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Whatsapp</th>
                                    <th>Poin</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($datas as $data )
                                    
                                <tr>
                                    <td>{{ $data->name }}</td>
                                    <td>{{ $data->email }}</td>
                                    <td>{{ $data->whatsapp}}</td>
                                    <td> {{ $data->time }}</td>
                                </tr>
            
                                @endforeach
                            </tbody>
                        </table>
                        
                    </div>
                     <!--/.box-body -->
                    {{-- <div class="text-center"> --}}
                        
                    {{-- </div> --}}
                </div>
                 <!--/.box -->
            </div>
        </div>
    </section>
@endsection

@section('jsPage')

@endsection