@extends("crudbooster::admin_template")

@section("content")
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Statistic User Harbolnas 11.11 by Colour</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Colour</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i = 1; @endphp
                                @foreach ($colours as $data )
                                    
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $data->colour }}</td>
                                    <td>{{ $data->total }}</td>
                                </tr>
                                @php $i++; @endphp
                                @endforeach
                            </tbody>
                        </table>
                        
                    </div>
                    <!-- /.box-body -->
                    {{-- <div class="text-center"> --}}
                        
                    {{-- </div> --}}
                </div>
                <!-- /.box -->
            </div>
            
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Statistic WhatsApp CS</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>CS Name</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i = 1; @endphp
                                @foreach ($cs as $data )
                                    
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $data->name }}</td>
                                    <td>{{ $data->total }}</td>
                                </tr>
                                @php $i++; @endphp
                                @endforeach
                            </tbody>
                        </table>
                        
                    </div>
                    <!-- /.box-body -->
                    {{-- <div class="text-center"> --}}
                        
                    {{-- </div> --}}
                </div>
                <!-- /.box -->
            </div>
            
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Statistic User Harbolnas 11.11</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Status</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i = 1; @endphp
                                @foreach ($datas as $data )
                                    
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $data->status }}</td>
                                    <td>{{ $data->total }}</td>
                                </tr>
                                @php $i++; @endphp
                                @endforeach
                            </tbody>
                        </table>
                        
                    </div>
                    <!-- /.box-body -->
                    {{-- <div class="text-center"> --}}
                        
                    {{-- </div> --}}
                </div>
                <!-- /.box -->
            </div>
            
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Statistic User Harbolnas 11.11 by Provinsi</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Provinsi</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i = 1; @endphp
                                @foreach ($provinsi as $p )
                                    
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $p->provinsi }}</td>
                                    <td>{{ $p->total }}</td>
                                </tr>
                                @php $i++; @endphp
                                @endforeach
                            </tbody>
                        </table>
                        
                    </div>
                    <!-- /.box-body -->
                    {{-- <div class="text-center"> --}}
                        
                    {{-- </div> --}}
                </div>
                <!-- /.box -->
            </div>
            
        </div>
@endsection