@extends("crudbooster::admin_template")

@section('content')
<div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">User List</h3>

            <div class="box-tools">
              <div class="input-group input-group-sm hidden-xs" style="width: 150px;">
                <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                <div class="input-group-btn">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
              </div>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Username</th>
                  <th>Email</th>
                  <th>Whatsapp</th>
                  <th>Kota</th>
                  <th>Alamat Rumah</th>
                  <th>Kode POS</th>
                  <th>Whatsapp Verification</th>
                  <th>Email Verification</th>
                  <th>Created At</th>
                </tr>
              </thead>
              <tbody>
                @forelse($users as $user)
                <tr>
                  <td>{{$user->name}}</td>
                  <td>{{$user->username}}</td>
                  <td>{{$user->email}}</td>
                  <td>{{$user->whatsapp}}</td>
                  <td>{{$user->kota}}</td>
                  <td>{{$user->alamat_rumah}}</td>
                  <td>{{$user->kode_pos}}</td>
                  <td>{{$user->whatsapp_verification == 1 ? 'Terverifikasi' : 'Belum verif'}}</td>
                  <td>{{$user->emailValidation == 'validated' ? 'Terverifikasi':'Belum verif'}}</td>
                  <td>{{$user->created_at}}</td>
                </tr>
                @empty
              <tr>
                <td colspan="10" class="text-center">
                  Data Kosong
                </td>
              </tr>
            </tbody>
              @endforelse
              <tfoot>
                <tr>
                  <td colspan="10" class="text-center">
                    {{$users->links()}}
                  </td>
                </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    </div>
@endsection
