@extends("crudbooster::admin_template")

@section('content')
  <!-- Main content -->
  <section class="content">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Search User</h3>
          </div>
        <div class="box-body">
            <form action="{{ route('userList') }}" method="GET">
                <div class="input-group">
                    <!-- /btn-group -->
                    <input type="text" name="search" class="form-control">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-danger">Search</button>
                      </div>
                  </div>
            </form>
        </div>
      </div>

      <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Daftar User</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Whatsapp</th>
                                <th>Alamat Rumah</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($datas as $data )
                                
                            <tr>
                                <td>{{ $data->name }}</td>
                                <td>{{ $data->email }}</td>
                                <td>{{ $data->whatsapp }}</td>
                                <td> {{ $data->alamat_rumah }}</td>
                            </tr>
        
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="4" class="text-center">
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                    
                </div>
                <!-- /.box-body -->
                {{-- <div class="text-center"> --}}
                    
                {{-- </div> --}}
            </div>
            <!-- /.box -->
        </div>
    </div>

  </section>
@endsection