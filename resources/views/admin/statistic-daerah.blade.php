@extends("crudbooster::admin_template")

@section("content")
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Statistic Daerah User Harbolnas 10.10</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Daerah</th>
                                    <th>Total Data</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i = 1; @endphp
                                @foreach ($datas as $data )
                                    
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $data->provinsi }}</td>
                                    <td>{{ $data->total }}</td>
                                </tr>
                                @php $i++; @endphp
                                @endforeach
                            </tbody>
                        </table>
                        
                    </div>
                    <!-- /.box-body -->
                    {{-- <div class="text-center"> --}}
                        
                    {{-- </div> --}}
                </div>
                <!-- /.box -->
            </div>
        </div>
@endsection