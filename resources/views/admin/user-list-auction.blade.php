@extends("crudbooster::admin_template")

@section('content')
  <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Daftar User Mengikuti Auction - {{$count}}</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Whatsapp</th>
                                    <th>Alamat Rumah</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($lists as $data )
                                    
                                <tr>
                                    <td>{{ $data->name }}</td>
                                    <td>{{ $data->email }}</td>
                                    <td>{{ $data->whatsapp }}</td>
                                    <td> {{ $data->alamat_rumah }}</td>
                                </tr>
            
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="4" class="text-center">
                                        {{ $lists->links() }}
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                        
                    </div>
                    <!-- /.box-body -->
                    {{-- <div class="text-center"> --}}
                        
                    {{-- </div> --}}
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
@endsection