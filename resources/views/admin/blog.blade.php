@extends("crudbooster::admin_template")
@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection

@section("content")
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Blogs</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Title</th>
                                    <th>Body</th>
                                    <th>Image</th>
                                    <!--<th>Status</th>-->
                                    <!--<th>Status accept</th>-->
                                </tr>
                            </thead>
                            <tbody>
                                @php $i = 1; @endphp
                                @foreach ($datas as $data )
                                    
                                <tr>
                                    <td>{{ $data->name }}</td>
                                    <td>{{ $data->email }}</td>
                                    <td>{{ $data->title }}</td>
                                    <td id="data{{$i}}"> <div id="checkId">{{ fillter($data->body) }}</div></td>
                                    <td><img src="{{ asset($data->image) }}" alt="image" class="img-thumbnail"></td>
                                    <!--<td>{{ $data->status }}</td>-->
                                    <!--<td>{{ $data->admin_acceptance }}</td>-->
                                </tr>
                                @php $i++; @endphp
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="4" class="text-center">
                                        {{ $datas->links() }}
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                        
                    </div>
                    <!-- /.box-body -->
                    {{-- <div class="text-center"> --}}
                        
                    {{-- </div> --}}
                </div>
                <!-- /.box -->
            </div>
        </div>
@endsection