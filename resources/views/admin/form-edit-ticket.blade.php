@section('title')
    Edit Tiket
@endsection

@extends("crudbooster::admin_template")

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            <strong><i class="fa fa-play"></i>Edit Tiket Spin It Yourself</strong>
        </div>
        {{-- BODY DISINI --}}
        <div class="panel-body">
            <!--begin::Container-->
            <form class="form" id="redeem-form" action="{{ route('submitEditTiket', ['userId'=>$id]) }}" method="POST">
                @csrf
                <div class="form-group row">
                    <div class="col-sm-3 col-12">
                        <label>Nama</label>
                    </div>
                    <div class="col-sm-9 col-12">
                        <input readonly type="text" class="form-control" name="nama" value='{{$name->name}}'/>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-3 col-12">
                        <label>Reguler</label>
                    </div>
                    <div class="col-sm-9 col-12">
                        <input type="number" class="form-control" name="reguler" value='{{$countTicketReguler}}'/>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-3 col-12">
                        <label>Premium</label>
                    </div>
                    <div class="col-sm-9 col-12">
                        <input type="number" class="form-control" name="premium" value='{{$countTicketPremium}}'/>
                    </div>
                </div>
                    <div class="form-group"> 
                        <div style="display: flex; justify-content: center;margin-top: 1rem">
                            <input type="submit" name="submit" value="Confirm" class="btn btn-success" style="width: 150px">
                        </div>
                    </div> 
            </form>
        </div>
    </div>

@endsection


@section('jsPage')

@endsection
