@extends('crudbooster::admin_template')
@section('content')
<div class='panel panel-default'>
    <div class='panel-heading'>Detail User</div>
        <div class='panel-body'>  
            <div class="table-responsive">
                <table id="table-detail" class="table table-striped">
                    <tbody>
                        <tr>
                            <th>Name</th>
                            <td>{{$row->name}}</td>
                        </tr>
                        <tr>
                            <th>Username</th>
                            <td>{{$row->username}}</td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td>{{$row->email}}</td>
                        </tr>
                        <tr>
                            <th>Whatsapp</th>
                            <td>{{$row->whatsapp}}</td>
                        </tr>
                        <tr>
                            <th>NIK</th>
                            <td>{{$row->nik}}</td>
                        </tr>
                        <tr>
                            <th>KTP</th>
                            <td><img src="https://sobatbadak.club/storage/ktp/{{ $row->ktp }}" alt="" style="height: 200px"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection