@section('title')
    Add Tiket
@endsection

@extends("crudbooster::admin_template")

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            <strong><i class="fa fa-play"></i>Add Tiket Spin It Yourself</strong>
        </div>
        {{-- BODY DISINI --}}
        <div class="panel-body">
            <!--begin::Container-->
            <form class="form" id="redeem-form" action="{{route('submitAddTiket')}}" method="POST">
                @csrf
                <div class="form-group row">
                    <div class="col-sm-3 col-12">
                        <label>Nama</label>
                    </div>
                    <div class="col-sm-9 col-12">
                       <select class="name form-control" style="width:100%;" name="name" required></select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-3 col-12">
                        <label>Premium</label>
                    </div>
                    <div class="col-sm-9 col-12">
                        <input type="number" class="form-control" name="reguler" value='{{$countTicketReguler}}'/>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-3 col-12">
                        <label>Premium</label>
                    </div>
                    <div class="col-sm-9 col-12">
                        <input type="number" class="form-control" name="premium" value='{{$countTicketPremium}}'/>
                    </div>
                </div>
                    <div class="form-group"> 
                        <div style="display: flex; justify-content: center;margin-top: 1rem">
                            <input type="submit" name="submit" value="Confirm" class="btn btn-success" style="width: 150px">
                        </div>
                    </div> 
            </form>
        </div>
    </div>

@endsection


@section('jsPage')
    <script src="{{ url('/') }}/vendor/crudbooster/assets/select2/dist/js/select2.full.min.js"></script>
    <script type="text/javascript">
        $('.name').select2({
        placeholder: 'Cari...',
        ajax: {
          url: '{{route('addUsers')}}',
          dataType: 'json',
          delay: 100,
          processResults: function (data) {
              console.log(data);
            return {
              results:  $.map(data, function (item) {
                return {
                  text: item.name,
                  id: item.id
                }
              })
            };
          },
          cache: true
        }
      });
    </script>
@endsection
