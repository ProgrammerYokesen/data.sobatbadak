@extends("crudbooster::admin_template")

@section('content')

<section class="content-header" style="margin-bottom: 15px">
  <h1>
    <i class="fa fa-eur"></i> Vouchers &nbsp;&nbsp;
    <a href="https://data.sobatbadak.club/admin/vouchers/add?return_url=https%3A%2F%2Fdata.sobatbadak.club%2Fadmin%2Fvoucher&amp;parent_id=&amp;parent_field=" id="btn_add_new_data" class="btn btn-sm btn-success" title="Add Data">
      <i class="fa fa-plus-circle"></i> Add Data
    </a>
  </h1>


  <ol class="breadcrumb">
    <li><a href="https://data.sobatbadak.club/admin"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Vouchers</li>
  </ol>
</section>


<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header" style="margin-bottom: 50px">
        <h3 class="box-title">Vouchers</h3>

        <div class="box-tools">
          <form action="{{ route('voucherList') }}" method="get" style="margin-bottom: 5px">
            @csrf
            <div class="input-group input-group-sm hidden-xs" style="width: 200px;">
                <input type="text" name="q" class="form-control pull-right" placeholder="Search">

                <div class="input-group-btn">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>

            </div>
          </form>

          <div class="text-right">
            <a href="{{route('voucherList')}}">
              <button class="btn">Clear Search</button>
            </a>
          </div>

        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>Voucher</th>
              <th>Poin</th>
              <th>Quantity</th>
              <th>Start Date</th>
              <th>End Date</th>
              <th>Status</th>
              <th>Penukaran</th>
              <th class="text-right">Action</th>
            </tr>
          </thead>
          <tbody>
              @forelse($data as $d)
                <tr>
                  <td>{{$d->voucher}}</td>
                  <td>{{$d->poin}}</td>
                  <td>{{$d->quantity}}</td>
                  <td>{{$d->start_date}}</td>
                  <td>{{$d->end_date}}</td>
                  <td>{{$d->status}}</td>
                  <td>{{$d->penukaran}}</td>
                  <td>
                    <div class="button_action" style="text-align:right">
                      <a class="btn btn-xs btn-success btn-edit" title="Edit Data" href="https://data.sobatbadak.club/admin/vouchers/edit/{{$d->id}}?return_url=https%3A%2F%2Fdata.sobatbadak.club%2Fadmin%2Fvoucher&amp;parent_id=&amp;parent_field="><i class="fa fa-pencil"></i></a>
                      <a class="btn btn-xs btn-warning btn-delete" title="Delete" href="javascript:;" onclick="swal({
                  				title: &quot;Are you sure ?&quot;,
                  				text: &quot;You will not be able to recover this record data!&quot;,
                  				type: &quot;warning&quot;,
                  				showCancelButton: true,
                  				confirmButtonColor: &quot;#ff0000&quot;,
                  				confirmButtonText: &quot;Yes!&quot;,
                  				cancelButtonText: &quot;No&quot;,
                  				closeOnConfirm: false },
                  				function(){  location.href=&quot;https://data.sobatbadak.club/admin/vouchers/delete/{{$d->id}}?return_url=https%3A%2F%2Fdata.sobatbadak.club%2Fadmin%2Fvoucher&amp;parent_id=&amp;parent_field=&quot; });"><i class="fa fa-trash"></i>
                      </a>
                    </div>
                  </td>
                </tr>
              @empty
                <tr>
                  <td colspan="10" class="text-center">
                    Data Kosong
                  </td>
                </tr>
              @endforelse
          </tbody>
          <tfoot>
            <tr>
              <td colspan="10" class="text-center">
                {{$data->links()}}
              </td>
            </tr>
          </tfoot>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
</div>
@endsection
