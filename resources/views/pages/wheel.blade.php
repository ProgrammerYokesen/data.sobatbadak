@section('title', 'Admin Wheel')

@section('style_assets')
<link href="{{ asset('css/gacha.css') }}" rel="stylesheet" type="text/css" />
@endsection

@extends('master')

@section('content')
<section id="wheel">
    <img class="img__badak" src="{{ asset('images/baper.png') }}" alt="">
    <img class="img__mamak" src="{{ asset('images/mamak.png') }}" alt="">
    <div class="container coin">
        <div class="card-custom gutter-b coin-card">
            <div class="text-center gacha-header">
                <img class="logo__spin" src="{{ asset('images/spin.png') }}" alt="">
                <p class="text-box-card">
                    {{ $data->invoice }} - {{ $data->nama }} - {{ $data->qtyTotal }} BUNDLE
                </p>
            </div>
            <div class="card-body">
                <div class="d-flex justify-content-center">
                    <div class="me-2">
                        <div id="box-regular" class="box-card" onclick="chooseWheel('box-regular')">
                            <div class="text-box-card">
                                Wheel Regular
                            </div>
                        </div>
                    </div>
                    <div class="ms-2">
                        <div id="box-premium" class="box-card" onclick="chooseWheel('box-premium')">
                            <div class="text-box-card">
                                Wheel Premium
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-center pt-4">
                    <canvas id="canvas__reg" width="450" height="450">
                        <p style="{color: white}" align="center">Sorry, your browser doesn't support canvas. Please
                            try
                            another.</p>
                    </canvas>
                    <canvas id="canvas__pre" width="450" height="450">
                        <p style="{color: white}" align="center">Sorry, your browser doesn't support canvas. Please
                            try
                            another.</p>
                    </canvas>
                </div>
                <div class="d-flex justify-content-center">
                    <div class="form-group row" style="width: 500px">
                        <div class="col-md-9 col-12 mt-2">
                            <input id="userEmail" type="email" class="form-control" placeholder="Masukan email user"
                                required />
                        </div>
                        <div class="col-md-3 col-12 mt-2 text-right">
                            <button type="submit" onclick="spinProcess();" id="submit__btn" class="btn btn_primer">
                                Spin
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal-->
<div class="modal fade" id="wheel-modal" data-backdrop="static" tabindex="-1" role="dialog"
    aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="d-flex justify-content-center" style="align-items: center">
                    <div class="circle-text">
                        BP
                    </div>
                    <div class="ml-2 big-text" style="margin:3rem 0">
                        x <span class="text-prize"></span>
                    </div>
                </div>
                <div class="">
                    <div class="title">
                        Selamat! Kamu mendapatkan <span class="text-prize"></span> Baper Poin
                    </div>
                    <div class="subtitle">
                        Yuk kumpulin terus dan menangkan lebih banyak <br>
                        lelangan di Club Sobat Badak!
                    </div>
                </div>
                <div class="d-flex justify-content-center" style="margin-top: 2rem">
                    <button onclick="elementID == 'canvas__pre' ? resetPremium() : resetRegular()"
                        class="redeem-button btn btn_primer" data-bs-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="confirm-modal" data-backdrop="static" tabindex="-1" role="dialog"
    aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="title" style="text-align: center;margin:1rem 0">
                    Yuk beli Koin Gatotkaca dan dapatkan Tiket Premium
                </div>
                <div class="subtitle">
                    Beli produk di Warisan Gajahmada dengan tanda khusus <a target="_blank"
                        style="color: #ffaa3a;text-decoration: underline"
                        href="https://www.tokopedia.com/warisangajahmada/paket-6pcs-edisi-gatotkaca-koin-larutan-penyegar-cap-badak-rasa-leci-leci">Spin
                        The Wheel</a> dan copy paste invoicemu
                    untuk mendapatkan Tiket Spin The Wheel
                </div>
                <div class="d-flex justify-content-center" style="margin-top: 2rem">
                    <button class="redeem-button btn btn_primer" data-bs-dismiss="modal">OK!</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="error-modal" data-backdrop="static" tabindex="-1" role="dialog"
    aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="">
                    <div class="title" style="text-align: center;margin:1rem 0">
                        Gagal Melakukan Spin!
                    </div>
                    <div class="subtitle">
                        Mohon maaf, sepertinya terjadi kesalahan saat melakukan spinwheel. Pastikan email yang kamu
                        masukan benar dan
                        memiliki tiket yang cukup!
                    </div>
                </div>
                <div class="d-flex justify-content-center" style="margin-top: 2rem">
                    <button class="redeem-button btn btn_primer" data-bs-dismiss="modal">OK!</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

{{-- ALL script javascript will be shown in master --}}
@section('scriptJS')
<script src="https://malsup.github.io/jquery.blockUI.js">
</script>
<script>
    var listPremium = <?php echo $premiumPrize; ?>;
    var listRegular = <?php echo $regularPrize; ?>;
    var countPremium = listPremium.length;
    var countRegular = listRegular.length;

    var elementID = "canvas__reg"
    var clickable = true;
    let bigButton = $('#submit__btn')

    function blockProgress() {
        $.blockUI({
            message: "<h3>Please wait...<h3>",
            css: {
                color: '#ffaa3a',
                borderColor: '#ffaa3a',
                backgroundColor: 'white',
                borderRadius: '10px',
                padding: '10px 20px'
            }
        });
    }

    function unblockProgress() {
        $.unblockUI();
    }

    $(function() {
        $('#canvas__pre').css("display", "none")
        $('#ticket__pre').css("display", "none")
        $("#box-regular").addClass('border__active')
        $('#ticket__reg').addClass("d-flex")
    })

    function chooseWheel(choosed) {
        if (clickable) {
            if (choosed.includes("regular")) {
                $("#box-regular").addClass('border__active')
                $("#box-premium").removeClass('border__active')
                $('#canvas__pre').css("display", "none")
                $('#canvas__reg').css("display", "block")

                $('#ticket__pre').css("display", "none")
                $('#ticket__reg').css("display", "block")
                $('#ticket__reg').addClass("d-flex")
                $('#ticket__pre').removeClass("d-flex")
                elementID = "canvas__reg"
            } else if (choosed.includes("premium")) {
                $("#box-premium").addClass('border__active')
                $("#box-regular").removeClass('border__active')
                $('#canvas__reg').css("display", "none")
                $('#canvas__pre').css("display", "block")

                $('#ticket__reg').css("display", "none")
                $('#ticket__pre').css("display", "block")
                $('#ticket__pre').addClass("d-flex")
                $('#ticket__reg').removeClass("d-flex")
                elementID = "canvas__pre"
            }
        }
    }

    // Make Color Generator
    function generateColor(idx) {
        let colors = ['#249924', '#3369e8', '#d50f26', '#eeb211']
        return colors[idx]
    }

    function triggerModal(res) {
        $(".text-prize").text(res.text)
        $("#wheel-modal").modal('toggle')
        clickable = true
    }

    function confirmModal() {
        $("#confirm-modal").modal('toggle');
        clickable = true;
        bigButton.disabled = false;
    }

    function errorModal() {
        $("#error-modal").modal('toggle');
        clickable = true;
        bigButton.disabled = false;
    }

    function preparingWheel() {
        blockProgress()
    }

    // AJAX handle bookmark and unbookmark 
    function spinProcess() {
        var email = document.getElementById("userEmail").value;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            // url: "route('spinWheelAdmin')",
            url: "{{ route('handleSpin') }}",
            type: "POST",
            data: {
                type: elementID == "canvas__reg" ? 2 : 1,
                email: email
            },
            beforeSend: function() {
                preparingWheel()
                clickable = false
            },
            success: function(res) {
                console.log(res)
                if (res.code == 0) {
                    unblockProgress()
                    errorModal()
                } else {
                    setTimeout(function() {
                        unblockProgress()
                        if (elementID === "canvas__reg") {
                            $("#ticket__reg .text-box-card").text(res.ticketReguler)
                            calculateRegular(res.sudut)
                        } else {
                            $("#ticket__pre .text-box-card").text(res.ticketPremium)
                            calculatePremium(res.sudut)
                        }
                    }, 2000);
                }
            },
            error: function(err) {
                console.log("ERROR")
                console.log(err)
                unblockProgress()
                errorModal()
            }
        })
    }

    //the wheel
    var i = 0
    let wheelRegular = new Winwheel({
        'canvasId': 'canvas__reg',
        'numSegments': countRegular,
        'outerRadius': 200,
        'segments': listRegular.map((val) => {
            if (i == 4) i = 0
            return {
                'fillStyle': generateColor(i++),
                'text': `${val.point}`
            }
        }),
        'animation': {
            'type': 'spinToStop',
            'duration': 5,
            'spins': countRegular,
            'callbackAfter': 'regularTriangle()',
            'callbackFinished': triggerModal
        }
    });
    i = 0
    let wheelPremium = new Winwheel({
        'canvasId': 'canvas__pre',
        'numSegments': countPremium,
        'outerRadius': 200,
        'segments': listPremium.map((val) => {
            if (i == 4) i = 0
            return {
                'fillStyle': generateColor(i++),
                'text': `${val.point}`
            }
        }),
        'animation': {
            'type': 'spinToStop',
            'duration': 5,
            'spins': countPremium,
            'callbackAfter': 'premiumTriangle()',
            'callbackFinished': triggerModal
        }
    });

    // Function with formula to work out stopAngle before spinning animation.
    // Called from Click of the Spin button.
    function calculateRegular(cons_position) {
        let stopAt = cons_position;
        wheelRegular.animation.stopAngle = stopAt;
        wheelRegular.startAnimation();
    }

    function calculatePremium(cons_position) {
        let stopAt = cons_position;
        wheelPremium.animation.stopAngle = stopAt;
        wheelPremium.startAnimation();
    }

    // Usual pointer drawing code.
    premiumTriangle();
    regularTriangle();

    function premiumTriangle() {
        // Get the canvas context the wheel uses.
        let ctx = wheelPremium.ctx;
        ctx.strokeStyle = 'navy'; // Set line colour.
        ctx.fillStyle = 'black'; // Set fill colour. 
        ctx.beginPath(); // Begin path.
        ctx.moveTo(170 + 25, 5); // Move to initial position.
        ctx.lineTo(230 + 25, 5); // Draw lines to make the shape.
        ctx.lineTo(200 + 25, 40);
        ctx.lineTo(171 + 25, 5);
        ctx.stroke(); // Complete the path by stroking (draw lines).
        ctx.fill(); // Then fill.
    }

    function regularTriangle() {
        // Get the canvas context the wheel uses.
        let ctx = wheelRegular.ctx;
        ctx.strokeStyle = 'navy'; // Set line colour.
        ctx.fillStyle = 'gray'; // Set fill colour. 
        ctx.beginPath(); // Begin path.
        ctx.moveTo(170 + 25, 5); // Move to initial position.
        ctx.lineTo(230 + 25, 5); // Draw lines to make the shape.
        ctx.lineTo(200 + 25, 40);
        ctx.lineTo(171 + 25, 5);
        ctx.stroke(); // Complete the path by stroking (draw lines).
        ctx.fill(); // Then fill.
    }

    // Function to reset the wheel
    function resetRegular() {
        wheelRegular.stopAnimation(false);
        wheelRegular.rotationAngle = 0;
        wheelRegular.draw();
        regularTriangle();
        bigButton.disabled = false;
    }

    function resetPremium() {
        wheelPremium.stopAnimation(false);
        wheelPremium.rotationAngle = 0;
        wheelPremium.draw();
        premiumTriangle();
        bigButton.disabled = false;
    }
</script>
@endsection
