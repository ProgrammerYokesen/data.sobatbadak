@section('title', 'Analytic Page')

@extends('master')

@section('content')
    <section id="analytic" class="body container">
        <div class="text-center title-page my-5">
            <h2 style="color: #fbaa3b"><b>Analytic Page</b></h2>
        </div>
        <div class="custom-animate">
            <div class="row justify-content-center mb-4">
                <div class="col-lg-4 col-md-6 mt-2 col-sm-8 col-12">
                    <div id="card-1" class="card custom-card" style="width: 18rem;">
                        <div class="card-body">
                            <h3 class="card-title">{{ number_format($real, 0, '.', '.') }}</h3>
                            <div class="card-subtitle">Date : <span
                                    class="date-bold">{{ date('d-m-Y H:i:s', strtotime('+ 7 hours')) }}</span></div>
                            <ul>
                                <li>
                                    <p class="card-text">IP Spam : {{ $spam }}
                                        ({{ number_format($percent_spam, 2, '.', '.') }}%)</p>
                                </li>
                                <li>
                                    <p class="card-text">Valid Email : {{ number_format($emailTrue, 0, '.', '.') }}
                                        ({{ number_format($percentTrue, 2, '.', '.') }}%)</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mt-2 col-sm-8 col-12">
                    <div id="card-2" class="card custom-card" style="width: 18rem;">
                        <div class="card-body">
                            <h3 class="card-title">{{ Session::get('real1') - Session::get('real2') }}</h3>
                            <div class="card-subtitle">Date : <span
                                    class="date-bold">{{ date('d-m-Y H:i:s', strtotime('+ 7 hours')) }}</span></div>
                            <ul>
                                <li>
                                    <p class="card-text">{{ $avg }} dpm</p>
                                </li>
                                <li>
                                    <p class="card-text">Forecast : {{ $forecast }} mph</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div> 
            </div>
            <div class="row justify-content-space-between">
                <div class="col-md-6 mt-2">
                    <table class="table table-hover">
                        <thead class="table-dark">
                            <tr>
                                <th scope="col" width="80%">Penghitungan Data</th>
                                <th style="text-align:right" width="20%" scope="col">Count</th>
                            </tr>
                        </thead>
                        <tbody> 
                                <tr>
                                    <th scope="row" width="80%">Register
                                    </th>
                                    <td width="20%" style="text-align:right">{{ $register }}</td>
                                </tr> 
                                <tr>
                                    <th scope="row" width="80%">Email Bersih
                                    </th>
                                    <td width="20%" style="text-align:right">{{ $emailBersih }}</td>
                                </tr> 
                                <tr>
                                    <th scope="row" width="80%">Whatsapp Verifikasi
                                    </th>
                                    <td width="20%" style="text-align:right">{{ $whatsappVerifikasi }}</td>
                                </tr> 
                                <tr>
                                    <th scope="row" width="80%">Isi Alamat Rumah
                                    </th>
                                    <td width="20%" style="text-align:right">{{ $alamatRumah }}</td>
                                </tr> 
                                <tr>
                                    <th scope="row" width="80%">Login Google
                                    </th>
                                    <td width="20%" style="text-align:right">{{ $loginGoogle }}</td>
                                </tr> 
                                <tr>
                                    <th scope="row" width="80%">Tebak Kata User Lama
                                    </th>
                                    <td width="20%" style="text-align:right">{{ $tebakKataUserLama }}</td>
                                </tr> 
                                <tr>
                                    <th scope="row" width="80%">Tebak Kata User Baru
                                    </th>
                                    <td width="20%" style="text-align:right">{{ $tebakKataUserBaru }}</td>
                                </tr> 
                                <tr>
                                    <th scope="row" width="80%">Rejeki Sobat User Lama
                                    </th>
                                    <td width="20%" style="text-align:right">{{ $rejekiSobatUserLama }}</td>
                                </tr> 
                                <tr>
                                    <th scope="row" width="80%">Rejeki Sobat User Baru
                                    </th>
                                    <td width="20%" style="text-align:right">{{ $rejekiSobatUserBaru }}</td>
                                </tr> 
                        </tbody>
                    </table>
                </div>
                
                <div class="col-md-6 mt-2">
                    <table class="table table-hover">
                        <thead class="table-dark">
                            <tr>
                                <th scope="col" width="80%">Nama Campaign</th>
                                <th style="text-align:right" width="20%" scope="col">Count</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($campaigns as $x => $ads) 
                                    <tr>
                                        <th scope="row" width="80%">{{ $ads->name ? $ads->name : 'No Name (NULL)' }}
                                        </th>
                                        <td width="20%" style="text-align:right">{{ $ads->count }}</td>
                                    </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="col-md-6 mt-2">
                    <table class="table table-hover" style="table-layout: fixed;">
                        <thead class="table-dark">
                            <tr>
                                <th width="80%" scope="col" style="overflow-wrap: break-word;">Nama Medium</th>
                                <th width="20%" style="overflow-wrap: break-word;text-align:right" scope="col">Count</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($mediums as $x => $ads) 
                                    <tr>
                                        <th scope="row" width="80%" style="overflow-wrap: break-word;">{{ $ads->name ? $ads->name : 'No Name (NULL)' }}
                                        </th>
                                        <td width="20%" style="overflow-wrap: break-word;text-align:right">{{ $ads->count }}</td>
                                    </tr> 
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="col-md-6 mt-2">
                    <table class="table table-hover">
                        <thead class="table-dark">
                            <tr>
                                <th width="80%" scope="col">Nama Source</th>
                                <th width="20%" style="text-align:right" scope="col">Count</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($sources as $x => $ads) 
                                    <tr>
                                        <th scope="row" width="80%">{{ $ads->name ? $ads->name : 'No Name (NULL)' }}
                                        </th>
                                        <td width="20%" style="text-align:right">{{ $ads->count }}</td>
                                    </tr> 
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="col-md-6 mt-2">
                    <table class="table table-hover">
                        <thead class="table-dark">
                            <tr>
                                <th width="80%" scope="col">Nama Content</th>
                                <th width="20%" style="text-align:right" scope="col">Count</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($contents as $x => $ads) 
                                    <tr>
                                        <th scope="row" width="80%">{{ $ads->name ? $ads->name : 'No Name (NULL)' }}
                                        </th>
                                        <td width="20%" style="text-align:right">{{ $ads->count }}</td>
                                    </tr> 
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection

{{-- ALL script javascript will be shown in master --}}
@section('scriptJS')

@endsection
