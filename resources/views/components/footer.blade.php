<div class="footer bg-white py-4 d-flex flex-lg-column" id="custom-footer">
    <div class="container d-flex flex-column flex-md-row align-items-center justify-content-between">
        <div class="text-dark order-2 order-md-1">
            <span class="font-weight-bold mr-2">2021©</span>
            <a href="https://sobatbadak.club/dashboard" target="_blank" class="footer-link">Club Sobat
                Badak</a>
        </div>
        <div class="nav nav-dark order-1 order-md-2">
            <p style="margin: 0">Disponsori oleh <a class="footer-link" href="https://warisangajahmada.com/" target="_blank">Warisan
                    Gajahmada</a></p>
        </div>
    </div>
</div>
