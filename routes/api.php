<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/getdata', 'DataController@getdata')->name('getdata');
Route::get('/getdata-day', 'DataController@getDataPerDay')->name('getDataPerDay');
Route::get('/getdata-harbolnas', 'DataController@getHarbolnasData')->name('getHarbolnasData');
Route::get('harbolnas-order', 'DataController@harbolnasOrder')->name('harbolnasOrder');
Route::get('harbolnas-order-prioritize', 'DataController@harbolnasOrderPrioritize')->name('harbolnasOrderPrioritize');
Route::get('harbolnas-order', 'DataController@harbolnasOrder')->name('harbolnasOrder');
Route::get('harbolnas1111-order-teratur', 'DataController@harbolnasSebelasTeratur')->name('harbolnasSebelasTeratur');
Route::get('harbolnas1111-order-ced-cirebon', 'DataController@harbolnasSebelasCedCirebon')->name('harbolnasSebelasCedCirebon');
Route::get('harbolnas1111-order-ced-solo', 'DataController@harbolnasSebelasCedSolo')->name('harbolnasSebelasCedSolo');
Route::get('harbolnas1111-order-ced-surabaya', 'DataController@harbolnasSebelasCedSurabaya')->name('harbolnasSebelasCedSurabaya');
Route::get('harbolnas1111-order-ced-bali', 'DataController@harbolnasSebelasCedBali')->name('harbolnasSebelasCedBali');
