<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::middleware('admin')->group(function () {
    Route::post('/spinwheel-poin', 'inputPoinController@spinwheelPoin')->name('spinwheelPoin');
    Route::get('/spinwheel-poin/cari', 'inputPoinController@searchEmail')->name('cariEmail');
    Route::get('/admin/undian', 'undianController@index')->name('undianHariIni');
    Route::get('/admin/get-undian', 'undianController@getData')->name('undianHariIniData');
    Route::get('/admin/riwayat-bp', 'adminController@index')->name('riwayatBP');
    Route::get('/admin/riwayat-bp-user', 'adminController@users')->name('riwayatBPUser');
    Route::get('/admin/riwayat-bp-user/{id}', 'adminController@userDetail')->name('riwayatBPuserDetail');
    Route::get('/admin/riwayat-ticket-gacha', 'adminController@indexTicketGacha')->name('riwayatTicketGacha');
    Route::get('/admin/riwayat-ticket-gacha-user', 'adminController@usersTicketGacha')->name('riwayatTicketGachaUser');
    Route::get('/admin/riwayat-ticket-gacha-user/{id}', 'adminController@userTicketGachaDetail')->name('riwayatTicketGachaUserDetail');
    Route::get('/admin/coin-management/{id}', 'adminController@coinManagementpage')->name('coinManagementpage');
    Route::get('calculate-bp/tidak-sesuai/{id}/{value}', 'adminController@calculateBPTidakSesuai')->name('calculateBPTidakSesuai');
    Route::post('/edit-tukar-koin', 'adminController@editTukarKoin')->name('editTukarKoin');
    Route::get('admin/edit-ticket/{userId}', 'adminController@editTiket')->name('editTiket');
    Route::post('/submit-edit-ticket/{userId}', 'adminController@submitEditTiket')->name('submitEditTiket');
    Route::get('admin/addTicket', 'adminController@addNewTicket')->name('addNewTicket');
    Route::get('/addTicket/users', 'adminController@addUsers')->name('addUsers');
    Route::post('/submit-add-ticket', 'adminController@submitAddTiket')->name('submitAddTiket');

    Route::get('/get-data-stw', 'DataController@getDataStw')->name('getDataStw');

    // Route::get('/analytics', 'dataAnalyticController@analyticPage')->name('analyticPage');
     Route::get('/analytics', 'dataAnalyticController@analyticPageNew')->name('analyticPage');

    // Route::get('/analytics-old', 'dataAnalyticController@analyticPage')->name('analyticPageNew');

    Route::get('/tambahan-analytics', 'dataAnalyticController@dataTambahan')->name('dataTambahanAnalytics');

    // Route::get('/analytics', 'dataAnalyticController@analyticPage')->name('analyticPage');

    Route::get('/admin/auction-user', 'adminController@userListAuction')->name('userListAuction');

    Route::get('/admin/user-list', 'adminController@userList')->name('userList');
    Route::post('/spin-the-wheel', 'GachaController@handleSpin')->name('handleSpin');
    Route::get('/admin/spin', 'GachaController@adminWheel')->name('adminWheel');

    Route::get('/admin/leaderboard-panjat-pinang', 'adminController@panjatLeaderboard')->name('panjatLeaderboard');
    Route::get('/admin/leaderboard-tambal-ban', 'adminController@leaderboardTambalBan')->name('leaderboardTambalBan');

    Route::get('/admin/blogs', 'adminController@blogs')->name('blogs');

    Route::get('/admin/harbolnas/statistic', 'adminController@statisticHarbolnas')->name('statisticHarbolnas');
    Route::get('/admin/harbolnas/daerah', 'adminController@statisticDaerahaa')->name('statisticDaerah');

    Route::get('admin/user-list', 'adminController@userLists')->name('userList');

    Route::post('admin/reject-sppa-video', 'adminController@rejectSppaVideo')->name('rejectSppaVideo');


    /** Route Refactor */
    Route::get('admin/voucher', 'Admin\VoucherController@index')->name('voucherList');

});

Route::get('lab/data-daerah', 'labController@groupingDaerah')->name('groupingDaerah');
Route::get('lab/leaderboard-bunda', 'labController@leaderBoardBunda')->name('leaderBoardBunda');
// Route::get('lab/convert_biru', 'labController@changeBiru')->name('changeBiru');

// route data controller
Route::get('harbolnas-to-gacha', 'DataController@harbolnasToGacha')->name('harbolnasToGacha');
// Route::get('harbolnas-order', 'DataController@harbolnasOrder')->name('harbolnasOrder');
// Route::get('harbolnas-order-prioritize', 'DataController@harbolnasOrderPrioritize')->name('harbolnasOrderPrioritize');

Route::get('verifikasi-ktp/{id}',  'VerificationKtpController@verifikasiKtp')->name('verifikasi-ktp');
Route::get('reject-ktp/{id}',  'VerificationKtpController@rejectKtp')->name('reject-ktp');

Route::get('get-user/{id}', 'adminController@getPeserta');
