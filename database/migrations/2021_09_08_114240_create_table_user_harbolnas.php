<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableUserHarbolnas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_harbolnas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('lokasi');
            $table->string('whatsapp', 50)->nullable();
            $table->text('alamat')->nullable();
            $table->string('status', 50)->default('new');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_harbolnas');
    }
}
