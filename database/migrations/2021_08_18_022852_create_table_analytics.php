<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableAnalytics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analytics', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('register')->nullable();
            $table->integer('email_bersih')->nullable();
            $table->integer('whatsapp_verifikasi')->nullable();
            $table->integer('alamat_rumah')->nullable();
            $table->integer('login_google')->nullable();
            $table->integer('tebak_kata_lama')->nullable();
            $table->integer('tebak_kata_baru')->nullable();
            $table->integer('rejeki_sobat_lama')->nullable();
            $table->integer('rejeki_sobat_baru')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analytics');
    }
}
