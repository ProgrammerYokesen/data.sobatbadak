<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class VoucherController extends Controller
{
    public function index(Request $request)
    {
      $data = DB::table('vouchers')
              ->select('vouchers.id', 'vouchers.voucher', 'vouchers.poin', 'vouchers.qty as quantity', 'vouchers.start_voucher as start_date', 'vouchers.end_voucher as end_date', 'vouchers.status', DB::raw('count(voucher_usages.voucher_id) as penukaran'))
              ->leftJoin('voucher_usages', 'voucher_usages.voucher_id', 'vouchers.id')
              ->orderByDesc('vouchers.created_at')
              ->groupBy('vouchers.id');
      if($request->has('q')){
        $data = $data->where('vouchers.voucher', 'like', '%'.$request->q.'%')->paginate(50);
      }
      else{
        $data = $data->paginate(50);
      }
      // dd($data);
      return view('admin-refactor.voucher', compact('data'));
    }
}
