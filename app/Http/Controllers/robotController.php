<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class robotController extends Controller
{
    
    public function patroliRejekiSobat(){
        $date = date('Y-m-d H:i:s');
        $maxPoin = DB::table('quiz_rejeki_sobat')
            ->select('quiz_rejeki_sobat.id as quizId', 'pertanyaan_rejeki_sobat.id as pertanyaan_id', 'pertanyaan_rejeki_sobat.pertanyaan', 'pertanyaan_rejeki_sobat.jawaban_a', 'pertanyaan_rejeki_sobat.jawaban_b', 'pertanyaan_rejeki_sobat.jawaban_c', 'pertanyaan_rejeki_sobat.jawaban_d')
            ->leftJoin('quiz_rejeki_sobat_list', 'quiz_rejeki_sobat_list.quiz_rejeki_sobat_id', 'quiz_rejeki_sobat.id')
            ->leftJoin('pertanyaan_rejeki_sobat', 'quiz_rejeki_sobat_list.pertanyaan_id', 'pertanyaan_rejeki_sobat.id')
            ->where('quiz_rejeki_sobat.status', 1)
            ->where('quiz_rejeki_sobat.start_date', '<', $date)
            ->where('quiz_rejeki_sobat.end_date', '>', $date)
            ->sum('poin');
        $pulls = DB::table('user_poins')
                        ->where('from', 'quiz rejeki sobat')
                        ->where('robot_check', 0)
                        ->whereDate('created_at', Carbon::today())
                        ->limit(100)
                        ->get();
        foreach($pulls as $pull){
            echo $pull->id."\n";
            if($pull->poin > $maxPoin){
                DB::table('user_poins')->where('id', $pull->id)->update([
                    'status' => 0,
                    'robot_check' => 1
                    ]);
            }
            else{
                   DB::table('user_poins')->where('id', $pull->id)->update([
                    'robot_check' => 1
                    ]);
            }
        }
    }
}
