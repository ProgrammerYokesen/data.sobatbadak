<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class GachaController extends Controller
{
    public function adminWheel(Request $request)
    {
        DB::table('orders')->where('id', $request->id)->update([
            'status_spinwheel' => 'joined'
        ]);

        if ($request->id) {
            $premiumPrize = DB::table('cat_gacha_points')->select('point')->where('cat_id', 1)->where('status', 1)->get();
            $regularPrize = DB::table('cat_gacha_points')->select('point')->where('cat_id', 2)->where('status', 1)->get();
            $data = DB::table('orders')->where('id', $request->id)->first();
            return view('pages.wheel', compact('premiumPrize', 'regularPrize', 'data'));
        }
        return redirect()->back();
    }

    public function spinwheelAlgorithmAdmin($id, $user_id)
    {
        $data = DB::table('cat_gacha_points')
            ->select('cat_gacha_points.id', 'cat_gacha_points.point', 'category_gacha.cat_name')
            ->join('category_gacha', 'category_gacha.id', 'cat_gacha_points.cat_id')
            ->where('cat_gacha_points.cat_id', $id)
            ->where('cat_gacha_points.status', 1)->get();
        $totalData = $data->count();

        $randomNumber = rand(1, $totalData); //2
        $index = $randomNumber - 1; //1

        $output = ($randomNumber / $totalData) * 360; // 32
        $i = (360 / $totalData) - 1; //15
        $rnd = rand(1, $i); //14
        $output = $output - $rnd; //18

        $data = $data[$index];
        $cat = DB::table('category_gacha')->where('id', $data->cat_id)->first();

        /**Change ticket */
        DB::table('ticket')->where('category_gatcha_id', $id)
            ->where('user_id', $user_id)
            ->where('spinwheel_status', 0)
            ->limit(1)
            ->update([
                'spinwheel_status' => 1
            ]);

        /**Insert to user poin */
        DB::table('user_poins')->insert([
            'user_id' => $user_id,
            'poin' => $data->point,
            'fk_id' => $data->id,
            'from' => 'Spin It Yourself - ' . $data->cat_name,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        /**Update total baper poin */

        $baperPoin = DB::table('users')->where('id', $user_id)->first()->baper_poin;
        $newPoint = $baperPoin + $data->point;
        DB::table('users')->where('id', $user_id)->update([
            'baper_poin' => $newPoint
        ]);


        return $output;
    }

    public function handleSpin(Request $request)
    {
        $user = DB::table('users')->where('email', $request->email)->first();
        // $isTicketEnough = DB::table('ticket')
        //     ->where('user_id', $user->id)
        //     ->where('category_gatcha_id', $request->type)
        //     ->where('spinwheel_status', 0)
        //     ->count();
        if ($user) {
            /**Algoritm gacha */

            $sudut = $this->spinwheelAlgorithmAdmin($request->type, $user->id);

            /**Get sisa ticket user */

            $ticketPremium = DB::table('ticket')
                ->where('user_id', $user->id)
                ->where('category_gatcha_id', 1)
                ->where('spinwheel_status', 0)
                ->count();

            $ticketReguler = DB::table('ticket')
                ->where('user_id', $user->id)
                ->where('category_gatcha_id', 2)
                ->where('spinwheel_status', 0)
                ->count();

            /**return json */

            return response()->json([
                'status' => 'Success',
                'code' => 1,
                'sudut' => $sudut,
                'ticketPremium' => $ticketPremium,
                'ticketReguler' => $ticketReguler
            ]);
        } else {
            return response()->json([
                'status' => 'Success',
                'code' => 0
            ]);
        }
    }
}
