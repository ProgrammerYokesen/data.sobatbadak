<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Client;

class DataController extends Controller
{
     public function getdata(){
         $dateNow = date('Y-m-d');
         $dateTime = $dateNow.' 15:00:00';
         $endTime = $dateNow.' 21:00:00';
         $currentTime = date('Y-m-d H:i:s');
        // $startTime = \DB::connection('csb')->table('event_time')->where('time','<=',$dateTime)->where('end_time', '>', $dateTime)->first();
        // $startTimeEvent = $startTime->time;
        
        if($currentTime <= $endTime){
            $clientGetData = new Client();
            $resGetData = $clientGetData->get("https://gudang.warisangajahmada.com/api/get-order/$dateTime")->getBody();
            $responseGetData = json_decode($resGetData);
            $data = $responseGetData->data;
            
            foreach($data as $key=>$value){
                $check = \DB::connection('csb')->table('orders')->where('invoice', $value->Invoice)->count();
                // dd($check);
                    if($check < 1){
                         $insert = \DB::connection('csb')->table('orders')->insertGetId([
                        'invoice'=> $value->Invoice,
                        'nama'=>$value->nmPembeli,
                        'email'=>$value->email,
                        'noHp'=>$value->noHp,
                        'platform'=>$value->platform,
                        'qtyTotal'=>0,
                        'created_at'=>$value->created_at,
                        // 'time_event_id'=>$startTime->id
                        ]);
                    // insert detail
                        $qty = 0;
                        $sku ='';
                        foreach($value->order as $eachOrder){
                            // dd($eachOrder);
                            $sku = $eachOrder->sku;
                            $qty = $qty + $eachOrder->qty;
                            $insertDetail = \DB::connection('csb')->table('order_detail')->insert([
                              'order_id'=>$insert,
                              'nama_item'=>$eachOrder->item,
                              'qty'=> $eachOrder->qty,
                              'product_id'=>$eachOrder->product_id,
                              'created_at'=>$value->created_at,
                              'sku'=>$eachOrder->sku,
                            ]);
                        }
                        $update = \DB::connection('csb')->table('orders')->where('invoice', $value->Invoice)->update([
                         'qtyTotal'=>$qty,
                         'sku'=>$sku
                        ]);
                        $data[$key]->qtyTotal = $qty;
                        
                    }
            }
            // dd($data);
            return response()->json($resGetData);
        }
        
        
    }
    
    public function getDataStw(){
        // $dateNow = date('Y-m-d');
        $clientGetData = new Client();
        $resGetData = $clientGetData->get("https://gudang.warisangajahmada.com/api/get-order-stw")->getBody();
        $responseGetData = json_decode($resGetData);
        $data = $responseGetData->data;
        
        foreach($data as $key=>$value){
                $check = \DB::connection('csb')->table('orders')->where('invoice', $value->Invoice)->count();
                // dd($check);
                    if($check < 1){
                         $insert = \DB::connection('csb')->table('orders')->insertGetId([
                        'invoice'=> $value->Invoice,
                        'nama'=>$value->nmPembeli,
                        'email'=>$value->email,
                        'noHp'=>$value->noHp,
                        'platform'=>$value->platform,
                        'qtyTotal'=>0,
                        'created_at'=>$value->created_at,
                        // 'time_event_id'=>$startTime->id
                        ]);
                    // insert detail
                        $qty = 0;
                        $sku ='';
                        foreach($value->order as $eachOrder){
                            // dd($eachOrder);
                            $sku = $eachOrder->sku;
                            $qty = $qty + $eachOrder->qty;
                            $insertDetail = \DB::connection('csb')->table('order_detail')->insert([
                              'order_id'=>$insert,
                              'nama_item'=>$eachOrder->item,
                              'qty'=> $eachOrder->qty,
                              'product_id'=>$eachOrder->product_id,
                              'created_at'=>$value->created_at,
                              'sku'=>$eachOrder->sku,
                            ]);
                        }
                        $update = \DB::connection('csb')->table('orders')->where('invoice', $value->Invoice)->update([
                         'qtyTotal'=>$qty,
                         'sku'=>$sku
                        ]);
                        $data[$key]->qtyTotal = $qty;
                        
                    }
            }
            // dd($data);
            echo 'sukses';
            return response()->json($responseGetData);
    }
    
    public function getDataPerDay(){
        $now = date('Y-m-d');
        $getDataText = \DB::connection('csb')->table('orders')->whereDate('created_at', $now)->select('nama', 'id', 'invoice')->get();
        $qtyTotal =0;
        foreach($getDataText as $key=>$value){
            $getDetail = \DB::connection('csb')->table('order_detail')->where('order_id', $value->id)->get();
            $qtyTotal = 0;
            foreach($getDetail as $index=>$item){
                // $getDataText[$key]->order[$index] = [
                // 'item'=>$item->nama_item,
                // 'qty'=>$item->qty,
                // 'sku'=>$item->sku
                // ];
                $qtyTotal = $qtyTotal + $item->qty;
            }
            $getDataText[$key]->qtyTotal = $qtyTotal;
            unset($getDataText[$key]->id);
        }
        dd($getDataText);
        return response()->json($getDataText);
    }
    
    public function getHarbolnasData(){
        $data = \DB::connection('csb')->table('user_harbolnas_1010')->where('status', 'join')->where('id', '>', 5935)->get();
        return response()->json($data);
    }
    
    public function harbolnasToGacha(){
        $getData = \DB::connection('csb')->table('user_harbolnas_1010')->where('status', 'join')->get();
        foreach($getData as $data){
            $insertOrder = \DB::connection('csb')->table('orders')->insertGetId([
                'invoice'=>$data->order_id,
                'sku'=>'HARBOLNAS',
                'nama'=>$data->nama,
                'email'=>$data->email,
                'noHp'=>$data->no_wa,
                'platform'=>'harbolnas',
                'qtyTotal'=>1,
                'status_spinwheel'=>'notjoined',
                'time_event_id'=>0,
                'display_status'=>0,
                'status_redeem'=>0,
                'created_at'=>date('Y-m-d H:i:s'),
            ]);
            $insertDetail = \DB::connection('csb')->table('order_detail')->insert([
                'order_id'=>$insertOrder,
                'nama_item' => '1 Bundle Larutan Penyegar Cap Badak',
                'qty' => 1,
                'SKU' => 'HARBOLNAS',
                'created_at' => date('Y-m-d H:i:s'),
            ]);
        }
        
        // dd($getData);
    }
    
    public function harbolnasOrder(){
        $getData = \DB::connection('csb')->table('user_harbolnas_1010')->where('status', 'join')->where('prioritize', 0)->get();
        // dd($getData);
        return response()->json($getData);
    }
    
    public function harbolnasOrderPrioritize(){
        $getData = \DB::connection('csb')->table('user_harbolnas_1010')->where('status', 'join')->where('prioritize', 1)->whereNotIn('order_id', ['Invoice-kTEiFyL', 'Invoice-7sk4bIw', 'Invoice-IQ64apZ', 'Invoice-Pq1jGh7', 'Invoice-cnd6Hin', 'Invoice-4N5Ouff', 'Invoice-vFRazOv', 'Invoice-ENcAtvt', 'Invoice-bV0T2OX', 'Invoice-ID9VR46', 'Invoice-QNcAAUe', 'Invoice-DQ79U5M', 'Invoice-XB2UMkA', 'Invoice-N3Fsxeu', 'Invoice-OnaDT6K', 'Invoice-ukKI0MF', 'Invoice-YHsGT9L', 'Invoice-K8JPrVM', 'Invoice-4wEp50M'])->get();
        return response()->json($getData);
        // dd($getData);
    }
    // 
    public function harbolnasSebelasTeratur(){
        $getData = \DB::connection('csb')->table('user_harbolnas_1111')->where('id', '>',3624)->whereIn('status', ['join', 'workshop'])->where('flag', 0)->whereIn('provinsi', ['ACEH','BEKASI', 'BANTEN', 'BENGKULU', 'jaktim', 'DKI JAKARTA', 'JAMBI', 'KEPULAUAN BANGKA BELITUNG', 'KEPULAUAN RIAU', 'LAMPUNG', 'RIAU', 'SUMATERA BARAT', 'SUMATERA SELATAN', 'SUMATERA UTARA'])->get();
        foreach($getData as $data){
            $update = \DB::connection('csb')->table('user_harbolnas_1111')->where('id', $data->id)->update([
                'flag'=>1
            ]);
            // dd('cek', $data);
        }
        return response()->json($getData);
    }
    
    public function harbolnasSebelasCedCirebon(){
        $getData = \DB::connection('csb')->table('user_harbolnas_1111')->where('flag', 1)->where('id', '>', 4495)->whereIn('status', ['join', 'workshop'])->whereIn('provinsi', ['JAWA BARAT'])->get();
        foreach($getData as $data){
            $update = \DB::connection('csb')->table('user_harbolnas_1111')->where('id', $data->id)->update([
                'flag'=>1
            ]);
            // dd('cek', $data);
        }
        return response()->json($getData);
    }
    public function harbolnasSebelasCedSolo(){
        $getData = \DB::connection('csb')->table('user_harbolnas_1111')->where('id', '>', 4656)->whereIn('status', ['join','workshop'])->where('flag', 1)->whereIn('provinsi', ['DI YOGYAKARTA', 'JAWA TENGAH'])->get();
        foreach($getData as $data){
            $update = \DB::connection('csb')->table('user_harbolnas_1111')->where('id', $data->id)->update([
                'flag'=>1
            ]);
            // dd('cek', $data);
        }
        return response()->json($getData);
    }
    public function harbolnasSebelasCedSurabaya(){
        $getData = \DB::connection('csb')->table('user_harbolnas_1111')->where('flag', 1)->where('id', '>', 4530)->whereIn('status', ['join', 'workshop'])->whereIn('provinsi', ['JAWA TIMUR', 'KALIMANTAN SELATAN','BALI', 'NUSA TENGGARA BARAT', 'SULAWESI BARAT', 'SULAWESI SELATAN', 'SULAWESI UTARA', 'KALIMANTAN BARAT', 'KALIMANTAN TENGAH', 'KALIMANTAN TIMUR'])->get();
        foreach($getData as $data){
            $update = \DB::connection('csb')->table('user_harbolnas_1111')->where('id', $data->id)->update([
                'flag'=>1
            ]);
            // dd('cek', $data);
        }
        return response()->json($getData);
    }
    public function harbolnasSebelasCedBali(){
        $getData = \DB::connection('csb')->table('user_harbolnas_1111')->where('status', 'join')->whereIn('provinsi', ['BALI', 'NUSA TENGGARA BARAT', 'SULAWESI BARAT', 'SULAWESI SELATAN', 'SULAWESI UTARA', 'KALIMANTAN BARAT', 'KALIMANTAN TENGAH', 'KALIMANTAN TIMUR'])->get();
        foreach($getData as $data){
            $update = \DB::connection('csb')->table('user_harbolnas_1111')->where('id', $data->id)->update([
                'flag'=>1
            ]);
            // dd('cek', $data);
        }
        return response()->json($getData);
    }
}
