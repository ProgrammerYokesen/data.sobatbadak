<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use CRUDBooster;

class adminController extends Controller
{
    public function index(){
        $datas = DB::table('user_poins')->select('users.name', 'users.email', 'user_poins.created_at', 'user_poins.poin','user_poins.from')
                ->join('users', 'users.id', 'user_poins.user_id')
                ->orderByDesc('user_poins.id')
                ->where('user_poins.status', 1)
                ->paginate(20);
        $totalPoin = DB::table('user_poins')->where('status', 1)->where('poin', '>', 0)->sum('poin');
        // dd($totalPoin);
        return view('admin.riwayat-baper-poin', compact('datas', 'totalPoin'));
    }
    public function users(){
        $datas = DB::table('user_poins')->groupBy('user_poins.user_id')
        ->select(DB::raw('sum(user_poins.poin) as sum'), 'users.email', 'users.name', 'users.id')
        ->where('user_poins.status', 1)
        ->join('users', 'users.id', 'user_poins.user_id')
        ->orderByDesc('sum')
        ->paginate(20);
        $totalPoin = DB::table('user_poins')->where('status', 1)->where('poin', '>', 0)->sum('poin');
        return view('admin.users-poin', compact('datas', 'totalPoin'));
    }
    public function userDetail($id){
        $datas = DB::table('user_poins')->join('users', 'users.id', 'user_poins.user_id')
                ->select('users.name', 'users.email', 'user_poins.created_at', 'user_poins.poin','user_poins.from')
                ->where('users.id', $id)
                ->where('user_poins.status', 1)
                ->orderByDesc('user_poins.id')
                ->paginate(20);
        $totalPoin = DB::table('user_poins')->where('status', 1)->where('poin', '>', 0)->sum('poin');
        return view('admin.users-poin-detail', compact('datas', 'totalPoin'));
    }
    public function userListAuction(){
        $lists = DB::table('auction_bids')
                ->select('users.id', 'users.name', 'users.email', 'users.whatsapp', 'users.alamat_rumah')
                ->join('users', 'users.id', 'auction_bids.user_id')
                ->groupBy('auction_bids.user_id')
                ->paginate(20);
        $count = DB::table('auction_bids')
                ->select('users.id', 'users.name', 'users.email', 'users.whatsapp', 'users.alamat_rumah')
                ->join('users', 'users.id', 'auction_bids.user_id')
                ->groupBy('auction_bids.user_id')
                ->get()->count();
        return view('admin.user-list-auction', compact('lists', 'count'));
    }

    public function indexTicketGacha(){
        $datas = DB::table('user_poins')->where('from', 'Spin It Yourself - Reguler')->orWhere('from', 'Spin It Yourself - Premium')->select('users.name', 'users.email', 'user_poins.created_at', 'user_poins.poin','user_poins.from', 'user_poins.fk_id')
                ->join('users', 'users.id', 'user_poins.user_id')
                ->orderByDesc('user_poins.id')
                ->where('user_poins.status', 1)
                ->paginate(20);
        // foreach($datas->items() as $key=>$value){
        //     $getCatName = DB::table('ticket')->where('ticket.id', $value->fk_id)->join('category_gacha', 'ticket.category_gatcha_id', 'category_gacha.id')->select('category_gacha.cat_name')->first();
        //     $datas->items()[$key]->jenis = $getCatName->cat_name;
        // }
        // dd($datas->items());
        return view('admin.history-tickets-gacha', compact('datas'));
    }
    public function usersTicketGacha(){
        $datas = DB::table('user_poins')->where('from', 'Spin It Yourself - Reguler')->orWhere('from', 'Spin It Yourself - Premium')->groupBy('user_poins.user_id')
                ->select(DB::raw('sum(user_poins.poin) as sum'), 'users.email', 'users.name', 'users.id')
                ->where('user_poins.status', 1)
                ->join('users', 'users.id', 'user_poins.user_id')
                ->get()->sortByDesc('sum');
        // dd($datas);
        return view('admin.users-poin-gacha', compact('datas'));
    }

    public function userTicketGachaDetail($id){
        $datas = DB::table('user_poins')->whereIn('from', array('Spin It Yourself - Reguler','Spin It Yourself - Premium'))->join('users', 'users.id', 'user_poins.user_id')
                ->select('users.name', 'users.email', 'user_poins.created_at', 'user_poins.poin','user_poins.from')
                ->where('users.id', $id)
                ->where('user_poins.status', 1)
                ->orderByDesc('user_poins.id')
                ->paginate(50);
        return view('admin.users-poin-detail-gacha', compact('datas'));
    }

    public function calculateBPTidakSesuai($id, $value){
        $getPoin = DB::table('koins')->where('id', $id)->first();
        $totalPoin = $getPoin->poin * $value;

        return response()->json($totalPoin);
    }

     public function coinManagementpage($id)
    {
        $getData = DB::table('penukaran_poins')->where('id', $id)->first();
        $getDate = date('Y-m-d', strtotime($getData->tanggal_status_redeem));
        $getKoins = DB::table('tukar_koins')->where('user_id', $getData->user_id)->where('periode', $getData->id)->get();
        $getAllkoin = DB::table('koins')->get();
        $totalPoin = 0;
        foreach($getKoins as $koin){
            $index = (int)$koin->koin - 1;
            $getAllkoin[$index]->qty = $koin->qty;
            $totalPoin = $totalPoin + $koin->poin;

        }
        // dd($getKoins, $getAllkoin[0]->id, $getData, $poin, $totalPoin);

        return view('admin.form-admin-coin', compact('getData', 'getAllkoin', 'totalPoin'));
    }

     public function editTukarKoin(Request $request){
        // dd($request);
        $getUserId = DB::table('penukaran_poins')->where('kode_penukaran', $request->kode)->first();
        if($request->radioes4 == "0"){
            DB::table('user_poins')->insert([
               'user_id'=>$getUserId->user_id,
               'fk_id'=>$getUserId->id,
               'poin'=>$request->jumlah_poin,
               'from'=>'Tukar Koin',
               'created_at'=>date('Y-m-d H:i:s')
            ]);

            $update = DB::table('penukaran_poins')->where('kode_penukaran', $request->kode)->update([
                    'status_redeem'=>'koin diterima',
                    'tanggal_status_redeem'=>date('Y-m-d H:i:s'),
                    'status_koin'=>'diterima sesuai',
                    'tanggal_status_koin'=>date('Y-m-d H:i:s'),
                    'status_baper_poin'=>'sudah diproses',
                    'tanggal_status_baper_poin'=>date('Y-m-d H:i:s'),
                ]);
            CRUDBooster::redirect('admin/penukaran_poins', 'poin telah ditambahkan', 'success');
        }else{
            //  DB::table('user_poins')->insert([
            //   'user_id'=>$getUserId->user_id,
            //   'fk_id'=>$getUserId->id,
            //   'poin'=>$request->gived_poin,
            //   'from'=>'Tukar Koin',
            //   'created_at'=>date('Y-m-d H:i:s')
            // ]);
            $update = DB::table('penukaran_poins')->where('kode_penukaran', $request->kode)->update([
                    'status_redeem'=>'koin diterima',
                    'tanggal_status_redeem'=>date('Y-m-d H:i:s'),
                    'status_koin'=>'diterima tidak sesuai',
                    'tanggal_status_koin'=>date('Y-m-d H:i:s'),
                    'status_baper_poin'=>'sudah diproses',
                    'tanggal_status_baper_poin'=>date('Y-m-d H:i:s'),
                ]);
            $getId = DB::table('penukaran_poins')->where('kode_penukaran', $request->kode)->first();
            $delete = DB::table('tukar_koins')->where('periode', $getId->id)->delete();
            foreach($request->except(['_token', 'nama', 'alamat', 'no_wa', 'email', 'kode', 'koin1', 'koin2', 'koin3', 'koin4', 'koin5', 'koin6', 'koin7', 'koin8', 'koin9', 'koin10', 'jumlah_poin', 'radioes4', 'gived_poin', 'submit']) as$key=>$value){
               if($value != 0){
                    $poin = DB::table('koins')->where('id', $key)->first();
                    $totalPoin = (int)$poin->poin * $value;
                    $insertNewTukarKoins = DB::table('tukar_koins')->insert([
                        'periode'=>$getId->id,
                        'user_id'=>$getId->user_id,
                        'koin'=>$key,
                        'qty'=>$value,
                        'poin'=>$totalPoin,
                        'penukaran_id'=>$getId->id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]);
               }
            }
             DB::table('user_poins')->insert([
              'user_id'=>$getUserId->user_id,
              'fk_id'=>$getUserId->id,
              'poin'=>$totalPoin,
              'from'=>'Tukar Koin',
              'created_at'=>date('Y-m-d H:i:s')
            ]);

            CRUDBooster::redirect('admin/penukaran_poins', 'poin telah ditambahkan', 'success');

        }
    }

    public function userList(Request $request){
        $datas = [];

        if($request->has('search')){
            $datas = DB::table('users')
                    ->where(function ($query) use ($request){
                        $query->where('name', 'like', '%'.$request->search.'%')
                              ->orWhere('email', 'like', '%'.$request->search.'%');
                    })
                    ->take(20)
                    ->get();
        }
        return view('admin.user-list', compact('datas'));
    }


    public function editTiket($id){
        $countTicketReguler = DB::table('ticket')->where('user_id', $id)->where('category_gatcha_id', 2)->where('spinwheel_status', 0)->count();
        $countTicketPremium = DB::table('ticket')->where('user_id', $id)->where('category_gatcha_id', 1)->where('spinwheel_status', 0)->count();
        $name = DB::table('users')->where('id', $id)->select('name')->first();
        // dd($name, $countTicketReguler, $countTicketPremium, $id);

        return view('admin.form-edit-ticket', compact('name', 'countTicketPremium', 'countTicketReguler', 'id'));
    }
    public function addNewTicket(){

        return view('admin.form-add-ticket');
    }
    public function submitEditTiket(Request $request, $userId){
        $deleteReguler = DB::table('ticket')->where('user_id', $userId)->where('category_gatcha_id', 2)->where('spinwheel_status', 0)->delete();
        $deletePremium = DB::table('ticket')->where('user_id', $userId)->where('category_gatcha_id', 1)->where('spinwheel_status', 0)->delete();
        $reguler = $request->reguler;
        $premium = $request->premium ;
        if($reguler > 0){
            for($i = 0; $i<$reguler; $i++){
                $insert = DB::table('ticket')->insert([
                    'order_id'=> 0,
                    'category_gatcha_id'=> 2,
                    'user_id'=> $userId,
                    'spinwheel_status'=> 0,
                    'created_at'=> date('Y-m-d H:i:s'),
                ]);
            }
        }
        if($premium > 0){
            for($i = 0; $i<$premium; $i++){
                $insert = DB::table('ticket')->insert([
                    'order_id'=> 0,
                    'category_gatcha_id'=> 1,
                    'user_id'=> $userId,
                    'spinwheel_status'=> 0,
                    'created_at'=> date('Y-m-d H:i:s'),
                ]);
            }
        }

        CRUDBooster::redirect('admin/ticket46', 'ticket telah diedit', 'success');
    }

    public function addUsers(Request $request){
        if ($request->has('q')) {
            $cari = $request->q;
            $data = DB::table('users')->where('name','LIKE', $cari.'%')->select('id', 'name')->get();
            return response()->json($data);
        }
    }

    public function submitAddTiket(Request $request){
        $reguler = $request->reguler;
        $premium = $request->premium;
        if($request->reguler > 0 ){
            for($i = 0; $i < $reguler; $i++){
                $insert = DB::table('ticket')->insert([
                    'order_id'=> 0,
                    'category_gatcha_id'=> 2,
                    'user_id'=> $request->name,
                    'spinwheel_status'=> 0,
                    'created_at'=> date('Y-m-d H:i:s'),
                ]);

            }
        }
        if($premium > 0){
            for($i = 0; $i<$premium; $i++){
                $insert = DB::table('ticket')->insert([
                    'order_id'=> 0,
                    'category_gatcha_id'=> 1,
                    'user_id'=> $request->name,
                    'spinwheel_status'=> 0,
                    'created_at'=> date('Y-m-d H:i:s'),
                ]);
            }
        }
        CRUDBooster::redirect('admin/ticket46', 'ticket telah ditambahkan', 'success');
    }

    public function panjatLeaderboard()
    {
        // $leaderBoard = DB::table('game_panjat_pinangs')
        //                 ->join('users', 'users.id', 'game_panjat_pinangs.user_id')
        //                 ->where('game_panjat_pinangs.status', 1)
        //                 ->where('game_panjat_pinangs.game_end', 1)
        //                 ->groupBy('game_panjat_pinangs.user_id')
        //                 ->limit(20)
        //                 ->orderByDesc('time')
        //                 ->get();
        // $leaderBoard = DB::table('game_panjat_pinangs')
        //          ->select('game_panjat_pinangs.user_id', 'users.name')
        //          ->join('users', 'users.id', 'game_panjat_pinangs.user_id')
        //          ->where('game_panjat_pinangs.status', 1)
        //          ->groupBy('user_id')
        //          ->get();
        // foreach($leaderBoard as $ky => $lb){
        //     $value = DB::table('game_panjat_pinangs')
        //             ->where('user_id', $lb->user_id)
        //             ->max('time');
        //     $leaderBoard[$ky]->time = $value;
        // }
        $leaderBoard = DB::table('game_panjat_pinangs')
                 ->select('game_panjat_pinangs.user_id', 'users.name', 'game_panjat_pinangs.created_at', 'users.whatsapp', 'users.email')
                 ->join('users', 'users.id', 'game_panjat_pinangs.user_id')
                 ->where('game_panjat_pinangs.status', 1)
                 ->groupBy('user_id')
                 ->get();
        foreach($leaderBoard as $ky => $lb){
            $value = DB::table('game_panjat_pinangs')
                    ->where('user_id', $lb->user_id)
                    ->where('game_panjat_pinangs.status', 1)
                    ->max('time');
            $leaderBoard[$ky]->time = $value;
        }

        $leaderBoard = $leaderBoard->sortByDesc('time')->values();
        $temp = [];
        foreach($leaderBoard as $key => $value){
            $Index = $key + 1;
            if($value->time == $leaderBoard[$Index]->time){
                $time1 = strtotime($value->created_at);
                $time2 = strtotime($leaderBoard[$Index]->created_at);
                if($time1 < $time2){
                    $temp = $leaderBoard[$key];
                    $leaderBoard[$key] = $leaderBoard[$Index];
                    $leaderBoard[$Index] = $temp;
                }
            }
        }
        $datas = $leaderBoard->sortByDesc('time')->values();
        $datas = $datas->take(20);
        return view('admin.leaderboard-panjat-pinang', compact('datas'));
    }

    public function leaderboardTambalBan()
    {
        $data['leaderboard'] = DB::table('pompa_ban')
            ->join('users', 'users.id', 'pompa_ban.user_id')
            ->where('pompa_ban.status', 1)->where('pompa_ban.game_end', 1)
            ->where('total_ban', 3)
            ->groupBy('pompa_ban.user_id')
            ->select('pompa_ban.user_id', 'users.name', 'users.whatsapp', 'users.email')
            ->orderBy('pompa_ban.time', 'asc')
            ->get();
        foreach($data['leaderboard'] as $key => $lb){
             $value = DB::table('pompa_ban')
                     ->where('total_ban', 3)
                     ->where('pompa_ban.status', 1)
                     ->where('pompa_ban.status', 1)
                     ->where('pompa_ban.game_end', 1)
                    ->where('user_id', $lb->user_id)
                    ->min('time');
            $data['leaderboard'][$key]->time = $value;
        }
        $datas = $data['leaderboard']->sortBy('time')->values();
        $datas = $datas->take(20);

        return view('admin.leaderboard-tambal-ban', compact('datas'));
    }

    public function blogs(){
        $datas = DB::table('blogs')
                ->join('users', 'users.id', 'blogs.user_id')
                ->select('blogs.id', 'users.name', 'users.email', 'blogs.title','blogs.body', 'blogs.image', 'blogs.status', 'blogs.admin_acceptance', 'blogs.created_at', 'blogs.updated_at')
                ->paginate(20);
        return view('admin.blog', compact('datas'));
    }

    public function statisticHarbolnas(){
        // $datas = DB::table('user_harbolnas')
        //          ->select('status', DB::raw('count(*) as total'))
        //          ->groupBy('status')
        //          ->get();

        // $data1010 = DB::table('user_harbolnas_1010')
        //          ->select('status', DB::raw('count(*) as total'))
        //          ->groupBy('status')
        //          ->get();

        // $makeup = DB::table('user_makeup')
        //          ->select('status', DB::raw('count(*) as total'))
        //          ->groupBy('status')
        //          ->get();

        $colours = DB::table('user_harbolnas_1111')
                    ->select('colour', DB::raw('count(colour) as total'))
                    ->groupBy('colour')
                    ->where('colour', '!=', null)
                    ->get();

        $cs = DB::table('user_harbolnas_1111')
                    ->join('whatsapp_cs', 'whatsapp_cs.id', 'user_harbolnas_1111.whatsapp_cs_id')
                    ->select('whatsapp_cs_id', 'whatsapp_cs.name', DB::raw('count(whatsapp_cs_id) as total'))
                    ->groupBy('user_harbolnas_1111.whatsapp_cs_id')
                    ->where('user_harbolnas_1111.status', 'join')
                    ->get();

        $datas = DB::table('user_harbolnas_1111')
                    ->select('status', DB::raw('count(*) as total'))
                    ->groupBy('status')
                    ->get();

        $provinsi = DB::table('user_harbolnas_1111')->select('provinsi', DB::raw('count(provinsi) as total'))->groupBy('provinsi')->get();

        return view('admin.statistic-harbolnas', compact('colours', 'cs', 'datas', 'provinsi'));
    }

    public function statisticDaerahaa(){
        $datas = DB::table('user_harbolnas_1010')
                 ->select('provinsi', DB::raw('count(*) as total'))
                 ->groupBy('provinsi')
                 ->where('status', '!=', 'new')
                 ->get();
        return view('admin.statistic-daerah', compact('datas'));
    }

    public function statisticWarna()
    {
        $datas = DB::table('user_harbolnas_1111')->select('colour', DB::raw('count(colour) as total'))->groupBy('browser')->get();
        dd($datas);
    }

    // public function statisticProvince()
    // {
    //     $datas = DB::table('user_harbolnas_1111')->select('provinsi', DB::raw('count(provinsi) as total'))->groupBy('provinsi')->get();
    //     dd($datas);
    // }

    public function userLists()
    {
      $users = DB::table('users')->paginate(50);
      return view('admin.user-lists', compact('users'));
    }

    public function rejectSppaVideo(Request $request){
      DB::table('sppa_videos')->where('id', $request->video_id)->update([
        'status' => 'rejected',
        'reason' => $request->reason,
        'updated_at' => now()
      ]);
      return redirect('https://data.sobatbadak.club/admin/sppa_videos')->with([
        "message" => "The status product has been updated !",
        "message_type" => "info"
      ]);
    }

    public function getPeserta($id)
    {
      $data = DB::table('harbolnas_1212_event_users')
              ->join('users', 'users.id', 'harbolnas_1212_event_users.user_id')
              ->join('harbolnas_1212_users', 'harbolnas_1212_users.user_id', 'harbolnas_1212_event_users.user_id')
              ->where('harbolnas_1212_event_users.event_id', $id)
              ->groupBy('users.id')
              ->select('users.email', 'harbolnas_1212_users.name')
              ->get();
      $totalData = count($data);
      echo "Total Peserta : $totalData".'<br><br><br>';
      foreach($data as $k => $v)
      {
        $txt = $v->email;
        $mail = explode("@",$txt);
        $len = strlen($mail[0]);
        $i = $len/2;
        $k = $len - 1;
        $r = '';
        for($j = 0 ;$j < $i ; $j++)
        {
          $r = $r.'*';
        }

        $replace = substr_replace($mail[0], $r, $i, $k);
        $email = $replace.'@'.$mail[1];
        echo $v->name.' - '.$email.'<br>';
      }
    }

}
