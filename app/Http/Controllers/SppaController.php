<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use CRUDBooster;

class SppaController extends Controller
{

  public function store(Request $request)
  {
    $check = DB::table('sppa_videos')->where('user_id', Auth::id())->where('status', 'accepted')->count();
    if($check == 0)
    {
      
    }
    else {
      return redirect()->back()->with([
        'success' => false,
        'message' => 'sudah upload video sebelumnya'
      ]);
    }
  }

}
