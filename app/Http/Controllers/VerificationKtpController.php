<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use CRUDBooster;

class VerificationKtpController extends Controller
{
    public function verifikasiKtp($id)
    {
        DB::table('users')->where('id', $id)->update([
            'ktp_verification' => 1
        ]);

        CRUDBooster::redirect('admin/users83', "KTP berhasil diverifikasi","success");
    }
    
    public function rejectKtp(Request $request, $id)
    {
        DB::table('users')->where('id', $id)->update([
            'ktp_verification' => 2,
        ]);

        CRUDBooster::redirect('admin/users83', "KTP berhasil ditolak","success");
    }
}
