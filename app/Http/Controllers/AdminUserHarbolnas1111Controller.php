<?php namespace App\Http\Controllers;

	use Session;
	use Request;
	use DB;
	use CRUDBooster;

	class AdminUserHarbolnas1111Controller extends \crocodicstudio\crudbooster\controllers\CBController {

	    public function cbInit() {

			# START CONFIGURATION DO NOT REMOVE THIS LINE
			$this->title_field = "nama";
			$this->limit = "20";
			$this->orderby = "id,desc";
			$this->global_privilege = true;
			$this->button_table_action = true;
			$this->button_bulk_action = true;
			$this->button_action_style = "button_icon";
			$this->button_add = false;
			$this->button_edit = true;
			$this->button_delete = false;
			$this->button_detail = true;
			$this->button_show = false;
			$this->button_filter = false;
			$this->button_import = false;
			$this->button_export = true;
			$this->table = "user_harbolnas_1111";
			# END CONFIGURATION DO NOT REMOVE THIS LINE

			# START COLUMNS DO NOT REMOVE THIS LINE
			$this->col = [];
			$this->col[] = ["label"=>"Nama","name"=>"nama"];
			$this->col[] = ["label"=>"No Wa","name"=>"no_wa"];
			$this->col[] = ["label"=>"Email","name"=>"email"];
			$this->col[] = ["label"=>"Alamat","name"=>"alamat"];
			$this->col[] = ["label"=>"Provinsi","name"=>"provinsi"];
			$this->col[] = ["label"=>"Status","name"=>"status"];
			$this->col[] = ["label"=>"colour","name"=>"colour"];
			$this->col[] = ["label"=>"Register From","name"=>"from"];
			$this->col[] = ["label"=>"Whatsapp Cs Id","name"=>"whatsapp_cs_id","join"=>"whatsapp_cs,name"];
			$this->col[] = ["label"=>"Tanggal Register","name"=>"created_at"];
			# END COLUMNS DO NOT REMOVE THIS LINE

			# START FORM DO NOT REMOVE THIS LINE
			$this->form = [];
			$this->form[] = ['label'=>'Nama','name'=>'nama','type'=>'text','validation'=>'required|string|min:3|max:70','width'=>'col-sm-10','placeholder'=>'You can only enter the letter only'];
			$this->form[] = ['label'=>'No Wa','name'=>'no_wa','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Email','name'=>'email','type'=>'email','validation'=>'required|min:1|max:255|email|unique:user_harbolnas_1111','width'=>'col-sm-10','placeholder'=>'Please enter a valid email address'];
			$this->form[] = ['label'=>'Alamat','name'=>'alamat','type'=>'textarea','validation'=>'required|string|min:5|max:5000','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Status','name'=>'status','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Whatsapp Cs Id','name'=>'whatsapp_cs_id','type'=>'select2','validation'=>'required|integer|min:0','width'=>'col-sm-10','datatable'=>'whatsapp_cs,name'];
			$this->form[] = ['label'=>'Ip Address','name'=>'ip_address','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Device','name'=>'device','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Screen','name'=>'screen','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Platform','name'=>'platform','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Version Platform','name'=>'version_platform','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Browser','name'=>'browser','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Languages','name'=>'languages','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Version Browser','name'=>'version_browser','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Order Id','name'=>'order_id','type'=>'select2','validation'=>'required|min:1|max:255','width'=>'col-sm-10','datatable'=>'order,id'];
			$this->form[] = ['label'=>'Prioritize','name'=>'prioritize','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Token','name'=>'token','type'=>'textarea','validation'=>'required|string|min:5|max:5000','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Provinsi','name'=>'provinsi','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Colour','name'=>'colour','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'From','name'=>'from','type'=>'text','validation'=>'required|min:1|max:255','width'=>'col-sm-10'];
			$this->form[] = ['label'=>'Usia','name'=>'usia','type'=>'number','validation'=>'required|integer|min:0','width'=>'col-sm-10'];
			# END FORM DO NOT REMOVE THIS LINE

			# OLD START FORM
			//$this->form = [];
			//$this->form[] = ["label"=>"Nama","name"=>"nama","type"=>"text","required"=>TRUE,"validation"=>"required|string|min:3|max:70","placeholder"=>"You can only enter the letter only"];
			//$this->form[] = ["label"=>"No Wa","name"=>"no_wa","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Email","name"=>"email","type"=>"email","required"=>TRUE,"validation"=>"required|min:1|max:255|email|unique:user_harbolnas_1111","placeholder"=>"Please enter a valid email address"];
			//$this->form[] = ["label"=>"Alamat","name"=>"alamat","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
			//$this->form[] = ["label"=>"Status","name"=>"status","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Whatsapp Cs Id","name"=>"whatsapp_cs_id","type"=>"select2","required"=>TRUE,"validation"=>"required|integer|min:0","datatable"=>"whatsapp_cs,name"];
			//$this->form[] = ["label"=>"Ip Address","name"=>"ip_address","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Device","name"=>"device","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Screen","name"=>"screen","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Platform","name"=>"platform","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Version Platform","name"=>"version_platform","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Browser","name"=>"browser","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Languages","name"=>"languages","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Version Browser","name"=>"version_browser","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Order Id","name"=>"order_id","type"=>"select2","required"=>TRUE,"validation"=>"required|min:1|max:255","datatable"=>"order,id"];
			//$this->form[] = ["label"=>"Prioritize","name"=>"prioritize","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Token","name"=>"token","type"=>"textarea","required"=>TRUE,"validation"=>"required|string|min:5|max:5000"];
			//$this->form[] = ["label"=>"Provinsi","name"=>"provinsi","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Colour","name"=>"colour","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"From","name"=>"from","type"=>"text","required"=>TRUE,"validation"=>"required|min:1|max:255"];
			//$this->form[] = ["label"=>"Usia","name"=>"usia","type"=>"number","required"=>TRUE,"validation"=>"required|integer|min:0"];
			# OLD END FORM

			/* 
	        | ---------------------------------------------------------------------- 
	        | Sub Module
	        | ----------------------------------------------------------------------     
			| @label          = Label of action 
			| @path           = Path of sub module
			| @foreign_key 	  = foreign key of sub table/module
			| @button_color   = Bootstrap Class (primary,success,warning,danger)
			| @button_icon    = Font Awesome Class  
			| @parent_columns = Sparate with comma, e.g : name,created_at
	        | 
	        */
	        $this->sub_module = array();


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Action Button / Menu
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @url         = Target URL, you can use field alias. e.g : [id], [name], [title], etc
	        | @icon        = Font awesome class icon. e.g : fa fa-bars
	        | @color 	   = Default is primary. (primary, warning, succecss, info)     
	        | @showIf 	   = If condition when action show. Use field alias. e.g : [id] == 1
	        | 
	        */
	        $this->addaction = array();
            $this->addaction[] = ['label'=>'Set Join','url'=>CRUDBooster::mainpath('set-status/[id]'),'icon'=>'fa fa-check','color'=>'success','showIf'=>"[status] == 'new'"];


	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add More Button Selected
	        | ----------------------------------------------------------------------     
	        | @label       = Label of action 
	        | @icon 	   = Icon from fontawesome
	        | @name 	   = Name of button 
	        | Then about the action, you should code at actionButtonSelected method 
	        | 
	        */
	        $this->button_selected = array();

	                
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add alert message to this module at overheader
	        | ----------------------------------------------------------------------     
	        | @message = Text of message 
	        | @type    = warning,success,danger,info        
	        | 
	        */
	        $this->alert        = array();
	                

	        
	        /* 
	        | ---------------------------------------------------------------------- 
	        | Add more button to header button 
	        | ----------------------------------------------------------------------     
	        | @label = Name of button 
	        | @url   = URL Target
	        | @icon  = Icon from Awesome.
	        | 
	        */
	        $this->index_button = array();



	        /* 
	        | ---------------------------------------------------------------------- 
	        | Customize Table Row Color
	        | ----------------------------------------------------------------------     
	        | @condition = If condition. You may use field alias. E.g : [id] == 1
	        | @color = Default is none. You can use bootstrap success,info,warning,danger,primary.        
	        | 
	        */
	        $this->table_row_color = array();     	          

	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | You may use this bellow array to add statistic at dashboard 
	        | ---------------------------------------------------------------------- 
	        | @label, @count, @icon, @color 
	        |
	        */
	        $this->index_statistic = array();



	        /*
	        | ---------------------------------------------------------------------- 
	        | Add javascript at body 
	        | ---------------------------------------------------------------------- 
	        | javascript code in the variable 
	        | $this->script_js = "function() { ... }";
	        |
	        */
	        $this->script_js = NULL;


            /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code before index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it before index table
	        | $this->pre_index_html = "<p>test</p>";
	        |
	        */
	        $this->pre_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include HTML Code after index table 
	        | ---------------------------------------------------------------------- 
	        | html code to display it after index table
	        | $this->post_index_html = "<p>test</p>";
	        |
	        */
	        $this->post_index_html = null;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include Javascript File 
	        | ---------------------------------------------------------------------- 
	        | URL of your javascript each array 
	        | $this->load_js[] = asset("myfile.js");
	        |
	        */
	        $this->load_js = array();
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Add css style at body 
	        | ---------------------------------------------------------------------- 
	        | css code in the variable 
	        | $this->style_css = ".style{....}";
	        |
	        */
	        $this->style_css = NULL;
	        
	        
	        
	        /*
	        | ---------------------------------------------------------------------- 
	        | Include css File 
	        | ---------------------------------------------------------------------- 
	        | URL of your css each array 
	        | $this->load_css[] = asset("myfile.css");
	        |
	        */
	        $this->load_css = array();
	        
	        
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for button selected
	    | ---------------------------------------------------------------------- 
	    | @id_selected = the id selected
	    | @button_name = the name of button
	    |
	    */
	    public function actionButtonSelected($id_selected,$button_name) {
	        //Your code here
	            
	    }


	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate query of index result 
	    | ---------------------------------------------------------------------- 
	    | @query = current sql query 
	    |
	    */
	    public function hook_query_index(&$query) {
	        //Your code here
	            
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate row of index table html 
	    | ---------------------------------------------------------------------- 
	    |
	    */    
	    public function hook_row_index($column_index,&$column_value) {	        
	    	//Your code here
	    	if($column_index == 7){
	    	    if($column_value == 'biru')
	    	    {
	    	        $column_value = "<span class='label label-info'>Biru</span>";
	    	    }
	    	    elseif($column_value == 'merah')
	    	    {
	    	        $column_value = "<span class='label label-danger'>Merah</span>";
	    	    }
	    	    elseif($column_value == 'kuning')
	    	    {
	    	        $column_value = "<span class='label label-warning'>Kuning</span>";
	    	    }
	    	    elseif($column_value == 'hijau')
	    	    {
	    	        $column_value = "<span class='label label-success'>Hijau</span>";
	    	    }
	    	    elseif($column_value == 'ungu'){
	    	        $column_value = "<span style='background-color: RGB(186, 17, 164)' class='label'>Ungu</span>";
	    	    }
	    	    elseif($column_value == 'jingga'){
	    	        $column_value = "<span style='background-color: RGB(224, 79, 0)' class='label'>Jingga</span>";
	    	    }
	    	    elseif($column_value == 'nila'){
	    	        $column_value = "<span style='background-color: RGB(76, 0, 130)' class='label'>Nila</span>";
	    	    }
	    	    
	    	    
	    	}
	    }

	    /*
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before add data is execute
	    | ---------------------------------------------------------------------- 
	    | @arr
	    |
	    */
	    public function hook_before_add(&$postdata) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after add public static function called 
	    | ---------------------------------------------------------------------- 
	    | @id = last insert id
	    | 
	    */
	    public function hook_after_add($id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for manipulate data input before update data is execute
	    | ---------------------------------------------------------------------- 
	    | @postdata = input post data 
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_edit(&$postdata,$id) {        
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after edit public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_edit($id) {
	        //Your code here 

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command before delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_before_delete($id) {
	        //Your code here

	    }

	    /* 
	    | ---------------------------------------------------------------------- 
	    | Hook for execute command after delete public static function called
	    | ----------------------------------------------------------------------     
	    | @id       = current id 
	    | 
	    */
	    public function hook_after_delete($id) {
	        //Your code here

	    }



	    //By the way, you can still create your own method in here... :) 

        public function getSetStatus($id) {
           
           
           //Generate Color Biru Merah Kuning jingga  hijau  nila ungu
           $lastColour = DB::table('user_harbolnas_1111')->where('colour', '!=', null)->orderBydesc('id')->first();
           if($lastColour){
               $user = DB::table('user_harbolnas_1111')->where('id', $id)->first();
               if($user->colour == null)
               {
                //   if($lastColour->colour == 'biru'){
                //       DB::table('user_harbolnas_1111')->where('id',$id)->update(['status'=>'join', 'colour' => 'kuning', 'updated_at' => now()]);
                //   }
                //     if($lastColour->colour == 'merah'){
                //       DB::table('user_harbolnas_1111')->where('id',$id)->update(['status'=>'join', 'colour' => 'kuning', 'updated_at' => now()]);
                //   }
                //     if($lastColour->colour == 'kuning'){
                //       DB::table('user_harbolnas_1111')->where('id',$id)->update(['status'=>'join', 'colour' => 'jingga', 'updated_at' => now()]);
                //   }
                //     if($lastColour->colour == 'jingga'){
                //       DB::table('user_harbolnas_1111')->where('id',$id)->update(['status'=>'join', 'colour' => 'hijau', 'updated_at' => now()]);
                //   }
                //   if($lastColour->colour == 'hijau'){
                    //   DB::table('user_harbolnas_1111')->where('id',$id)->update(['status'=>'join', 'colour' => 'biru', 'updated_at' => now()]);
                //   }
                //   if($lastColour->colour == 'nila'){
                //       DB::table('user_harbolnas_1111')->where('id',$id)->update(['status'=>'join', 'colour' => 'ungu', 'updated_at' => now()]);
                //   }
                //   if($lastColour->colour == 'ungu'){
                //       DB::table('user_harbolnas_1111')->where('id',$id)->update(['status'=>'join', 'colour' => 'biru', 'updated_at' => now()]);
                //   }
                $totalBiru = DB::table('user_harbolnas_1111')->where('colour', 'biru')->count();
                $totalHijau = DB::table('user_harbolnas_1111')->where('colour', 'hijau')->count();
                $totalKuning = DB::table('user_harbolnas_1111')->where('colour', 'kuning')->count();
                $totalJingga = DB::table('user_harbolnas_1111')->where('colour', 'jingga')->count();
                $totalMerah = DB::table('user_harbolnas_1111')->where('colour', 'merah')->count();
                if($totalMerah <450){
                    DB::table('user_harbolnas_1111')->where('id',$id)->update(['status'=>'join', 'colour' => 'merah', 'updated_at' => now()]);
                }elseif($totalHijau <450){
                    DB::table('user_harbolnas_1111')->where('id',$id)->update(['status'=>'join', 'colour' => 'hijau', 'updated_at' => now()]);
                }elseif($totalKuning <450){
                    DB::table('user_harbolnas_1111')->where('id',$id)->update(['status'=>'join', 'colour' => 'kuning', 'updated_at' => now()]);
                }elseif($totalJingga <450){
                    DB::table('user_harbolnas_1111')->where('id',$id)->update(['status'=>'join', 'colour' => 'jingga', 'updated_at' => now()]);
                }else{
                    DB::table('user_harbolnas_1111')->where('id',$id)->update(['status'=>'join', 'colour' => 'ungu', 'updated_at' => now()]);
                }
              }
               else
               {
                   DB::table('user_harbolnas_1111')->where('id',$id)->update(['status'=>'join', 'updated_at' => now()]);
               }
           }else{
               DB::table('user_harbolnas_1111')->where('id',$id)->update(['status'=>'join', 'colour' => 'biru', 'updated_at' => now()]);
           }
           
           
           
           //This will redirect back and gives a message
           CRUDBooster::redirect($_SERVER['HTTP_REFERER'],"The status product has been updated !","info");
        }
        
        
	}