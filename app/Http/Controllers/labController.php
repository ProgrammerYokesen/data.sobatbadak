<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class labController extends Controller
{
    public function groupingDaerah(){
        // $datasCount = DB::table('user_harbolnas_1010')->where('status', '!=', 'new')->count();
        // dd($datasCount);
        // $datas = DB::table('user_harbolnas_1010')->where('status', '!=', 'new')->get();
        $dataKota = DB::table('kota')->get();
        $lainnya = 0;
        foreach($dataKota as $key => $value){
            $total = 0;
            $verify   = $value->kota_name;
            $datas = DB::table('user_harbolnas_1010')->where('migrate', 0)->where('status', 'join')->get();
            foreach($datas as $dt){
                if($dt->migrate == 0)
                {
                    $key = strtolower($dt->alamat);
                    $search = strtolower($value->kota_name);
                    $checkVerify = strpos($key, $search);
                    if ($checkVerify !== false){
                        echo "masuk sini";
                        $total++;
                    }
                    else{
                        $provinsi_name = DB::table('provinsi')->where('id', $value->provinsi_id)->first()->name;
                        $key = strtolower($dt->alamat);
                        $search = strtolower($provinsi_name);
                        $checkVerify = strpos($key, $search);
                        if ($checkVerify !== false){
                            echo "masuk bawah";
                            $total++;
                        }
                        else{
                            $lainnya++;
                        }
                    }
                    DB::table('user_harbolnas_1010')->where('id', $dt->id)->update(['migrate' => 1]);
                }
            }
            $amount = DB::table('provinsi')->where('id', $value->provinsi_id)->first()->amount;
            $new_amount = $amount + $total;
            DB::table('provinsi')->where('id', $value->provinsi_id)->update([
                'amount' => $new_amount
                ]);
        }
        DB::table('provinsi')->where('id', 6)->update([
                'amount' => $lainnya
                ]);
        return 'success'.$lainnya;
    }

    public function leaderBoardBunda(){
        $data = DB::table('user_harbolnas_1010')->where('status', '!=', 'new')->get();
        foreach($data as $key => $value){
            $quiz1 = DB::table('quiz_bunda_starts')->where('status_pengerjaan', 1)
                    ->where('quiz_bunda_id', 1)
                    ->where('user_id', $value->id)
                    ->first();
            $quiz2 = DB::table('quiz_bunda_starts')->where('status_pengerjaan', 1)
                    ->where('quiz_bunda_id', 2)
                    ->where('user_id', $value->id)
                    ->first();
            $quiz3 = DB::table('quiz_bunda_starts')->where('status_pengerjaan', 1)
                    ->where('quiz_bunda_id', 3)
                    ->where('user_id', $value->id)
                    ->first();

            $poin = ($quiz1->nilai + $quiz2->nilai + $quiz3->nilai);
            $time1 = strtotime($quiz1->created_at);
            $time2 = strtotime($quiz2->created_at);
            $time3 = strtotime($quiz3->created_at);
            $time = $time1 + $time2 + $time3;
            // $time = (strtotime($quiz1->created_at) + strtotime($quiz2->created_at) + strtotime($quiz3->created_at));
            $data[$key]->poinRataRata = $poin;
            $data[$key]->time = $time;
        }
        $data = $data->sortByDesc('poinRataRata');
        $maxPoin = $data->max('poinRataRata');
        $data = $data->where('poinRataRata', $maxPoin);
        $data = $data->sortBy('time')->take(20);
        dd($data);
        dd($data, 'sini');
        foreach($data as $key => $value){
            DB::table('quiz_bunda_leaderboards')->insert([

            ]);
        }
    }

    public function changeBiru(){
      // cek

      $data = DB::table('user_harbolnas_1111')->where('colour', 'biru')->orderByDesc('updated_at')->limit(200)->get()->toArray();
      $chunk = array_chunk($data, 34);
      // hijau
      $userHijau = collect($chunk[0]);
      foreach ($userHijau as $key => $value) {
        // code...
        DB::table('user_harbolnas_1111')->where('id', $value->id)->update([
          'new_colour' => 'hijau'
        ]);
      }
      // jingga
      $userJingga = collect($chunk[1]);
      foreach ($userJingga as $key => $value) {
        // code...
        DB::table('user_harbolnas_1111')->where('id', $value->id)->update([
          'new_colour' => 'jingga'
        ]);
      }
      // kuning
      $userKuning = collect($chunk[2]);
      foreach ($userKuning as $key => $value) {
        // code...
        DB::table('user_harbolnas_1111')->where('id', $value->id)->update([
          'new_colour' => 'kuning'
        ]);
      }
      // merah
      $userMerah = collect($chunk[3]);
      foreach ($userMerah as $key => $value) {
        // code...
        DB::table('user_harbolnas_1111')->where('id', $value->id)->update([
          'new_colour' => 'merah'
        ]);
      }
      // ungu
      $userUngu = collect($chunk[4]);
      foreach ($userUngu as $key => $value) {
        // code...
        DB::table('user_harbolnas_1111')->where('id', $value->id)->update([
          'new_colour' => 'ungu'
        ]);
      }
      // nila
      $userNila = collect($chunk[5]);
      foreach ($userNila as $key => $value) {
        // code...
        DB::table('user_harbolnas_1111')->where('id', $value->id)->update([
          'new_colour' => 'nila'
        ]);
      }
    }

}
