<?php

function set_active($uri, $output = "active")
{
 if( is_array($uri) ) {
   foreach ($uri as $u) {
     if (Route::is($u)) {
       return $output;
     }
   }
 } else {
   if (Route::is($uri)){
     return $output;
   }
 }
}

function fillter($string){
    $badwords = config('badword');
    $text = strtolower($string);
    foreach($badwords as $badword){
        $lowerBadword = strtolower($badword); 
        $text = str_replace($lowerBadword, '****', $text);
    }
    // ... and so on
    return $text;
}